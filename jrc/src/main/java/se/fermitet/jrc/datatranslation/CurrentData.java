package se.fermitet.jrc.datatranslation;

import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.datatranslation.property.AttributeProperty;
import se.fermitet.jrc.datatranslation.property.CompoundProperty;
import se.fermitet.jrc.datatranslation.property.Property;
import se.fermitet.jrc.restparameter.ParameterData;
import se.fermitet.jrc.restparameter.SortParameter;

import java.util.*;
import java.util.stream.Collectors;


class CurrentData {
    private Object data;
    private ParameterData URLParameters;
    private Collection<Property> selectionProperties;

    CurrentData(Object data, ParameterData URLParameters) {
        super();
        this.data = data;
        this.URLParameters = URLParameters;
        this.selectionProperties = new ArrayList<>();

        if (this.URLParameters != null) initSelectionProperties();
    }

    Object getData() {
        return this.data;
    }

    boolean isCollection() {
        return (data instanceof Collection<?>);
    }

    boolean isMap() {
        return (data instanceof Map<?,?>);
    }

    boolean isEntity() {
        if (getData() == null) return false;
        return getData().getClass().isAnnotationPresent(Entity.class);
    }

    @SuppressWarnings("unchecked")
    Object getSingleData() {
        if (getData() == null) return null;

        Object aValue = null;

        if (this.isMap()) {
            Map<Object, Object> mData = this.getDataAsMap();
            if (mData.isEmpty()) return null;

            Object aKey = mData.keySet().iterator().next();
            aValue = mData.get(aKey);
        } else if(this.isCollection()) {
            Collection<Object> cData = this.getDataAsCollection();
            if (cData.isEmpty()) return null;

            aValue = cData.iterator().next();
        } else {
            aValue = getData();
        }

        if ((aValue instanceof Map<?, ?>) || (aValue instanceof Collection<?>))
            aValue = new CurrentData(aValue, null).getSingleData();

        return aValue;
    }

    Class<?> getClassOfSingleData() {
        Object dataExample = getSingleData();
        if (dataExample == null) return null;
        return dataExample.getClass();
    }

    Collection<Object> getDataAsCollection() {
        if (getData() == null) throw new IllegalArgumentException("Trying to treat data as a collection when it was null");
        if (!this.isCollection()) throw new IllegalArgumentException("Trying to treat data as a collection when it is a " + getData().getClass().getName());

        return (Collection<Object>) getData();
    }

    Map<Object, Object> getDataAsMap() {
        if (getData() == null) throw new IllegalArgumentException("Trying to treat data as a map when it was null");
        if (!this.isMap()) throw new IllegalArgumentException("Trying to treat data as a map when it is a " + getData().getClass().getName());
        return (Map<Object, Object>) getData();
    }

    Collection<Property> getSelectionProperties() {
        return this.selectionProperties;
    }

    List<SortParameter> getSortParameters() {
        if (this.URLParameters == null) return Collections.emptyList();
        return this.URLParameters.getSortParameters();
    }

    Collection<CurrentData> moveIntoCollection() {
        return this.getDataAsCollection().stream()
                .map(dataInCollection -> createCurrentDataForSubData(dataInCollection, this))
                .sorted((o1, o2) -> getPropertyValueComparatorResult(o1.getData(), o2.getData()))
                .collect(Collectors.toList());
    }

    Map<Object, CurrentData> moveIntoMap() {
        Map<Object, CurrentData> ret = new LinkedHashMap<>();

        for (Object key : getSortedListOfMapKeys()) {
            CurrentData currentDataForSubData = createCurrentDataForSubData(this.getDataAsMap().get(key), this);
            if (currentDataForSubData != null && currentDataForSubData.getSortParameters() != null) {
                currentDataForSubData.setSortParameters(currentDataForSubData.getSortParameters().stream()
                        .collect(Collectors.toList())
                );
            }
            ret.put(key, currentDataForSubData);
        }

        return ret;
    }

    CurrentData moveIntoProperty(Property related) {
        Object value = related.getValue(this.getData());

        CurrentData ret = new CurrentData(value, null);

        ret.setSelectionProperties(stripSelectionProperties(related, value));

        ret.setSortParameters(stripSortParameters(related.getName()));

        return ret;
    }

    private List<Property> stripSelectionProperties(Property related, Object value) {
        String propertyName = related.getName();
        return this.getSelectionProperties().stream()
                .filter(p -> p instanceof CompoundProperty)
                .filter(p -> {
                    return ((CompoundProperty) p).getMovementName().equals(propertyName);
                })
                .map(p -> {
                    CompoundProperty c = (CompoundProperty) p;
                    Object relatedObjectNotMapOrCollection = new CurrentData(value, null).getSingleData();
                    if (relatedObjectNotMapOrCollection != null)
                        return Property.getByName(c.getRelatedName(), relatedObjectNotMapOrCollection.getClass());
                    else
                        return null;
                })
                .collect(Collectors.toList());
    }

    private List<SortParameter> stripSortParameters(String propertyName) {
        if (this.getSortParameters() == null || this.getSortParameters().isEmpty()) return null;

        List<SortParameter> value = this.getSortParameters().stream()
                .filter(p -> p.getRelatedParamter() != null)
                .filter(p -> {
                    String name = p.getSortCollection();
                    String firstPartOfName = name.substring(0, name.contains(".") ? name.indexOf(".") : name.length());
                    return firstPartOfName.equals(propertyName);
                })
                .map(p -> p.getRelatedParamter())
                .collect(Collectors.toList());

        return value;
    }

    private List<Object> getSortedListOfMapKeys() {
        return this.getDataAsMap().keySet().stream()
                .sorted((k1, k2) -> getMapComparatorResult(k1, k2))
                .collect(Collectors.toList());
    }

    private int getMapComparatorResult(Object key1, Object key2) {
        if (getSortParameters() == null || getSortParameters().isEmpty()) return 0;

        return getPropertyValueComparatorResult(this.getDataAsMap().get(key1), this.getDataAsMap().get(key2));
    }

    @SuppressWarnings("unchecked")
    private int getPropertyValueComparatorResult(Object o1, Object o2) {
        if (this.getSortParameters() == null) return 0;

        for (SortParameter sortParameter : this.getSortParameters()) {
            if (sortParameter.getSortCollection() != null) continue;

            Property prop = Property.getByName(sortParameter.getSortByName(), this.getClassOfSingleData());

            Comparable<Object> value1 = (Comparable<Object>) prop.getValue(o1);
            Comparable<Object> value2 = (Comparable<Object>) prop.getValue(o2);

            if (value1 == null && value2 == null) continue;

            int directionMultiplier = sortParameter.isAscending() ? 1 : -1;

            int result;
            if (value1 == null) {
                result = directionMultiplier;
            } else if (value2 == null) {
                result = -1 * directionMultiplier;
            } else {
                result = value1.compareTo(value2) * directionMultiplier;
            }

            if (result != 0)
                return result;
        }
        return 0;
    }

    private CurrentData createCurrentDataForSubData(Object subData, CurrentData superCurrentData) {
        CurrentData currentDataForSubData = new CurrentData(subData, null);
        currentDataForSubData.setSelectionProperties(this.getSelectionProperties());
        currentDataForSubData.setSortParameters(this.getSortParameters());

        return currentDataForSubData;
    }

    private void initSelectionProperties() {
        if (this.getClassOfSingleData() == null) return;

        if (this.URLParameters.getAttributeNames() != null) {
            this.selectionProperties.addAll(getPropertiesFromNames(URLParameters.getAttributeNames()));
        }

        if (this.URLParameters.getRelationNames() != null) {
            this.selectionProperties.addAll(getPropertiesFromNames(URLParameters.getRelationNames()));
        }

        addDefaultSelectionPropertiesIfNeeded();
    }

    private void addDefaultSelectionPropertiesIfNeeded() {
        boolean hasAttributeProperties = this.selectionProperties.stream()
                .filter(p -> (p instanceof AttributeProperty)).findFirst().isPresent();

        if (!hasAttributeProperties) {
            this.selectionProperties.addAll(Property.getDefaultProperties(getClassOfSingleData()));
        }
    }

    private Collection<Property> getPropertiesFromNames(Collection<String> names) {
        return names.stream()
                .map(name -> Property.getByName(name, this.getClassOfSingleData()))
                .collect(Collectors.toList());
    }

    private void setSelectionProperties(Collection<Property> selectionProperties) {
        this.selectionProperties = selectionProperties;
        this.addDefaultSelectionPropertiesIfNeeded();
    }

    private void setSortParameters(List<SortParameter> sortParameters) {
        if (this.URLParameters == null) this.URLParameters = new ParameterData();
        this.URLParameters.setSortParameters(sortParameters);
    }

}
