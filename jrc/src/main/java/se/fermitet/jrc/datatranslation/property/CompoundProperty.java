package se.fermitet.jrc.datatranslation.property;


public class CompoundProperty extends Property {
	private String movementName;
	private String relatedName;
	private Property movementProperty;
	private Property relatedProperty;

	public CompoundProperty(String name,  Class<?> clz) {
		super(name, clz);

		if (!Property.isNameCompound(name))
			throw new IllegalArgumentException("You cannot create a compound property with the name " + name + " since there is no period in the name");

		int indexOfFirstPeriod = name.indexOf(".");

		this.movementName = name.substring(0, indexOfFirstPeriod);
		this.movementProperty = Property.getByName(getMovementName(), this.getClz());
		this.relatedName = name.substring(indexOfFirstPeriod + 1, name.length());
	}

	@Override
	public Object getValue(Object objectThatHasProperty) {
		try {
			ensureRelatedProperty(objectThatHasProperty);

			Object movedObject = this.movementProperty.getValue(objectThatHasProperty);
			return relatedProperty.getValue(movedObject);
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	@Override
	public void setValue(Object newValue, Object objectThatHasProperty) {
		throw new UnsupportedOperationException("setValue is unsupported on CompoundProperties");
	}

	public String getMovementName() {
		return this.movementName;
	}

	public String getRelatedName() {
		return this.relatedName;
	}

	private void ensureRelatedProperty(Object objectThatHasProperty) {
		if (this.relatedProperty != null) return;

		Object movedObject = this.movementProperty.getValue(objectThatHasProperty);
		this.relatedProperty = Property.getByName(getRelatedName(), movedObject.getClass());
	}
}