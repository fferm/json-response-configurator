package se.fermitet.jrc.datatranslation.property;

import se.fermitet.jrc.annotation.Attribute;

public class AttributeProperty extends Property {
	public AttributeProperty(String name, Class<?> clz) {
		super(name, clz);

		if (! super.checkPropertyAndSuperclassesCorrespondingPropertyForAnnotation(Attribute.class))
			throw new IllegalArgumentException("The property with name: " + super.getName() + " in class: " + clz.getName() + " is not an attribute");
	}

}