package se.fermitet.jrc.datatranslation;

import java.util.*;

import javax.ejb.Stateless;

import se.fermitet.jrc.datatranslation.property.CompoundProperty;
import se.fermitet.jrc.datatranslation.property.Property;
import se.fermitet.jrc.restparameter.ParameterData;

@Stateless
public class JRCDataTranslator {

    /**
     * @throws IllegalArgumentException if there is a problem with the attributes or relations of if the getter throws an exception
     * @return
     */
	public Object createJson(Object input, ParameterData URLParameters) {
	    if (input == null) return null;

	    CurrentData dataFromInput = new CurrentData(input, URLParameters);
        if (dataFromInput.getSingleData() == null) return null;

        return createJsonFromData(dataFromInput);
	}
	
    @SuppressWarnings("unchecked")
    private Object createJsonFromData(CurrentData currentData) {
	    if (currentData == null || currentData.getData() == null) return null;

        if (currentData.isCollection()) {
            return createJsonFromCollection(currentData);
        } else if (currentData.isMap()) {
            return createJsonFromMap(currentData);
        } else {
            return createJsonFromObject(currentData);
        }
    }

    private Object createJsonFromObject(CurrentData currentData) {
        if (currentData.isEntity()) {
            return createJsonFromEntity(currentData);
        } else {
            return currentData.getData();
        }
    }

    private Map<String, Object> createJsonFromEntity(CurrentData currentData) {
        Map<String, Object> map = new HashMap<>();

        for (Property currentSelectionProperty : currentData.getSelectionProperties()) {
            if (currentSelectionProperty instanceof CompoundProperty)
                continue;

            Object jsonObject = createJsonFromData(currentData.moveIntoProperty(currentSelectionProperty));
            map.put(currentSelectionProperty.getName(), jsonObject);
        }
        return map;
    }

    private List<?> createJsonFromCollection(CurrentData currentData) {
        List<Object> ret = new ArrayList<>();

        for (CurrentData currentDataFromInsideCollection : currentData.moveIntoCollection()) {
            ret.add(createJsonFromData(currentDataFromInsideCollection));
        }

        return ret;
    }

    private Map<?, ?> createJsonFromMap(CurrentData currentData) {
        Map<Object, Object> ret = new LinkedHashMap<Object, Object>();

        Map<Object, CurrentData> mapOfSubCurrentDatas = currentData.moveIntoMap();
        for (Object key : mapOfSubCurrentDatas.keySet()) {
            ret.put(key, createJsonFromData(mapOfSubCurrentDatas.get(key)));
        }

        return ret;
    }
}
