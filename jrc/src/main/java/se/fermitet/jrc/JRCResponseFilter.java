package se.fermitet.jrc;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import se.fermitet.jrc.datatranslation.JRCDataTranslator;
import se.fermitet.jrc.datatranslation.property.PayloadProperty;
import se.fermitet.jrc.restparameter.ParameterData;

@Provider
public class JRCResponseFilter implements ContainerResponseFilter {
	@Inject
	private ParameterData restParameterData;
	
	@Inject
	private JRCDataTranslator jrcDataTranslator;
	
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		if (! hasCorrectMediaType(responseContext)) return;

		Object entity = responseContext.getEntity();

		if (PayloadProperty.hasDefinedPayload(entity.getClass())) {
			PayloadProperty prop = PayloadProperty.getPayloadProperty(entity.getClass());
			Object mapped = jrcDataTranslator.createJson(prop.getValue(entity), restParameterData);
		
			prop.setValue(mapped, entity);

		} else {
			Object mapped = jrcDataTranslator.createJson(entity, restParameterData);
			
			responseContext.setEntity(mapped);
		}
	}

	private boolean hasCorrectMediaType(ContainerResponseContext responseContext) {
		MediaType mediaType = responseContext.getMediaType();
		return (mediaType != null) && (mediaType.equals(MediaType.APPLICATION_JSON_TYPE));
	}

}
