package se.fermitet.jrc.restparameter;

public class SortParameter {
	private String sortByName;
	private String sortCollection;
	private boolean ascending;
	private SortParameter relatedParameter;

	public SortParameter(String sortByProperty) {
		this(null, sortByProperty);
	}

	public SortParameter(String sortCollection, String sortByProperty) {
		super();

		this.ascending = !sortByProperty.startsWith("-");
		String sortByPropertyWithoutMinus = this.ascending ? sortByProperty : sortByProperty.substring(1, sortByProperty.length());

		this.sortByName = sortByPropertyWithoutMinus;

		initCollection(sortCollection, sortByProperty);
	}

	private void initCollection(String sortCollection, String sortByProperty) {
		this.sortCollection = sortCollection;

		if (this.getSortCollection() == null || this.getSortCollection().isEmpty()) return;

		String newCollection = null;
		if (this.getSortCollection().contains(".")) {
			int idx = this.getSortCollection().indexOf(".");
			newCollection = this.getSortCollection().substring(idx + 1);
		}

		this.relatedParameter = new SortParameter(newCollection, sortByProperty);
	}

	public String getSortByName() {
		return this.sortByName;
	}

	public boolean isAscending() {
		return this.ascending;
	}

	public boolean isDescending() {
		return !this.isAscending();
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("SortRestParameter -- ");

		buf.append("SortByName: ");
		buf.append(getSortByName());

		buf.append("    ");

		if (isAscending())
			buf.append("ASCENDING");
		else
			buf.append("DESCENDING");

		if (this.getSortCollection() != null) {
			buf.append("    Collection: ");
			buf.append(getSortCollection());
		}

		return buf.toString();
	}

	public String getSortCollection() {
		return this.sortCollection;
	}

	public SortParameter getRelatedParamter() {
		return this.relatedParameter;
	}


}
