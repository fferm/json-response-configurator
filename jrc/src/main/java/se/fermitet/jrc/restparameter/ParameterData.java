package se.fermitet.jrc.restparameter;

import java.util.Collection;
import java.util.List;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class ParameterData {
	private Collection<String> attributeNames;
	private Collection<String> relationNames;
	private List<SortParameter> sortParameters;

	public Collection<String> getAttributeNames() {
		return attributeNames;
	}

	public void setAttributeNames(Collection<String> attributeNames) {
		this.attributeNames = attributeNames;
	}

	public Collection<String> getRelationNames() {
		return relationNames;
	}

	public void setRelationNames(Collection<String> relationNames) {
		this.relationNames = relationNames;
	}

	public List<SortParameter> getSortParameters() {
		return sortParameters;
	}

	public void setSortParameters(List<SortParameter> sortParameters) {
		this.sortParameters = sortParameters;
	}


}
