package se.fermitet.jrc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import se.fermitet.jrc.annotation.Payload;
import se.fermitet.jrc.datatranslation.JRCDataTranslator;
import se.fermitet.jrc.restparameter.ParameterData;

@RunWith(MockitoJUnitRunner.class)
public class JRCResponseFilterTest {

	@Mock
	private JRCDataTranslator jsonEntityCreator;
	
	@Mock
	private ParameterData restParameterData;

	@Mock
	private ContainerResponseContext responseContext;
	
	@InjectMocks
	private JRCResponseFilter jrcResponseFilter;
	
	@Test
	public void notCalledWithNonJsonMediaType() throws Exception {
		when(responseContext.getMediaType()).thenReturn(MediaType.TEXT_PLAIN_TYPE);

		jrcResponseFilter.filter(null, responseContext);

		verify(responseContext, never()).setEntity(anyObject());
		verify(jsonEntityCreator, never()).createJson(anyObject(), anyObject());
	}
	
	@Test
	public void notCalledWhenThereIsNoMediaType() throws Exception {
		when(responseContext.getMediaType()).thenReturn(null);

		jrcResponseFilter.filter(null, responseContext);

		verify(responseContext, never()).setEntity(anyObject());
		verify(jsonEntityCreator, never()).createJson(anyObject(), anyObject());
	}
	
	@Test
	public void callsSetEntityWithChangedData_nonPayloadCase() throws Exception {
		Object changed = "CHANGED";
		Object original = "ORIGINAL";
	
		when(responseContext.getEntity()).thenReturn(original);
		when(responseContext.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
		when(jsonEntityCreator.createJson(original, restParameterData)).thenReturn(changed);
		
		jrcResponseFilter.filter(null, responseContext);
		
		verify(responseContext).setEntity(changed);
	}
	
	@Test
	public void payloadCase() throws Exception {
		class PayloadCarrier {
			@Payload private Object payload;
			public Object getPayload() { return payload; }
			public void setPayload(Object payload) { this.payload = payload; }
		}
		
		String dataOriginal = "ORIGINAL";
		String dataChanged = "CHANGED";
		PayloadCarrier obj = new PayloadCarrier();
		obj.setPayload(dataOriginal);
		
		when(responseContext.getEntity()).thenReturn(obj);
		when(responseContext.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
		when(jsonEntityCreator.createJson(eq(dataOriginal), any())).thenReturn(dataChanged);

		jrcResponseFilter.filter(null, responseContext);

		Object newPayload = ((PayloadCarrier) responseContext.getEntity()).getPayload();
		
		assertThat(newPayload, is(dataChanged));
	}
	
	@Test
	public void payloadCase_restParameterDataUsedOnPayload() throws Exception {
		class PayloadCarrier {
			@Payload private Object payload;
			@SuppressWarnings("unused") public Object getPayload() { return payload; }
			public void setPayload(Object payload) { this.payload = payload; }
		}
		
		String dataOriginal = "ORIGINAL";
		String dataChanged = "CHANGED";
		PayloadCarrier obj = new PayloadCarrier();
		obj.setPayload(dataOriginal);
		
		when(responseContext.getEntity()).thenReturn(obj);
		when(responseContext.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
		when(jsonEntityCreator.createJson(dataOriginal, restParameterData)).thenReturn(dataChanged);

		jrcResponseFilter.filter(null, responseContext);
		
		verify(jsonEntityCreator).createJson(any(), eq(restParameterData));
	}


}
