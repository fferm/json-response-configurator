package se.fermitet.jrc.restparameter;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

public class ParameterDataTest {
	@Test
	public void ParameterData_hasValidConstructor() {
		assertThat(ParameterData.class, hasValidBeanConstructor());
	}

	@Test
	public void ParameterData_hasValidGetterAndSetters() throws Exception {
		assertThat(ParameterData.class, hasValidGettersAndSetters());
	}
}
