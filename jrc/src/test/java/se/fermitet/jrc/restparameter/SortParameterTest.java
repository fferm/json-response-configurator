package se.fermitet.jrc.restparameter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class SortParameterTest {
	@Test
	public void ascendingWithName() throws Exception {
		SortParameter param = new SortParameter("name");
		assertThat(param.getSortByName(), is("name"));
		assertThat(param.isAscending(), is(true));
		assertThat(param.isDescending(), is(false));
		assertNull(param.getSortCollection());
	}

	@Test
	public void descendingWithName() throws Exception {
		SortParameter param = new SortParameter("-name");
		assertThat(param.getSortByName(), is("name"));
		assertThat(param.isAscending(), is(false));
		assertThat(param.isDescending(), is(true));
		assertNull(param.getSortCollection());
	}

	@Test
	public void withCollection() throws Exception {
		SortParameter param = new SortParameter("collection", "name");
		assertThat(param.getSortByName(), is("name"));
		assertThat(param.isAscending(), is(true));
		assertThat(param.isDescending(), is(false));

		assertThat(param.getSortCollection(), is("collection"));
	}

	@Test
	public void relatedParameter() throws Exception {
		SortParameter orig = new SortParameter("collection", "name");
		SortParameter related = orig.getRelatedParamter();

		assertThat(related.getSortByName(), is("name"));
		assertThat(related.isAscending(), is(true));
		assertNull(related.getSortCollection());
	}

	@Test
	public void relatedParameter_onRegular() throws Exception {
		SortParameter orig = new SortParameter("name");
		assertNull(orig.getRelatedParamter());
	}

	@Test
	public void relatedParameter_double() throws Exception {
		SortParameter orig = new SortParameter("collection1.collection2", "-prop");
		SortParameter related = orig.getRelatedParamter();

		assertThat(related.getSortByName(), is("prop"));
		assertThat(related.isAscending(), is(false));
		assertThat(related.getSortCollection(), is("collection2"));
	}

}
