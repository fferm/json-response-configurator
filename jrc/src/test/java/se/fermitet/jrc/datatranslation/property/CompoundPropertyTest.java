package se.fermitet.jrc.datatranslation.property;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import se.fermitet.jrc.datatranslation.property.PropertyTest.TestDoubleSourceClass;
import se.fermitet.jrc.datatranslation.property.PropertyTest.TestSourceClass;


public class CompoundPropertyTest {
	@Test
	public void getValue() throws Exception {
		TestSourceClass testObject = new TestSourceClass();

		Property compoundGetAttribute = new CompoundProperty("attributeLink.getAttribute", testObject.getClass());
		assertThat(compoundGetAttribute.getValue(testObject), is(testObject.getAttributeLink().getGetAttribute()));

		Property compoundIsAttribute = new CompoundProperty("attributeLink.isAttribute", testObject.getClass());
		assertThat(compoundIsAttribute.getValue(testObject), is(testObject.getAttributeLink().isIsAttribute()));

		Property compoundHasAttribute = new CompoundProperty("attributeLink.hasAttribute", testObject.getClass());
		assertThat(compoundHasAttribute.getValue(testObject), is(testObject.getAttributeLink().hasHasAttribute()));
	}

	@Test
	public void getValue_doubleLink() throws Exception {
		TestDoubleSourceClass testObject = new TestDoubleSourceClass();
		CompoundProperty compound = new CompoundProperty("doubleLink.attributeLink.getAttribute", testObject.getClass());

		assertThat(compound.getValue(testObject), is(testObject.getDoubleLink().getAttributeLink().getGetAttribute()));
	}

	@Test
	public void getValue_movementNull() throws Exception {
		TestSourceClass testObject = new TestSourceClass();
		testObject.attributeLink = null;

		Property compound = new CompoundProperty("attributeLink.getAttribute", testObject.getClass());
		assertNull(compound.getValue(testObject));
	}

	@Test(expected = IllegalArgumentException.class)
	public void illegalWithoutPeriodInName() throws Exception {
		new CompoundProperty("stringWitoutPeriod", TestSourceClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void illegalIfThereIsNoAttributeOrRelationCorrespondingToFirstPartOfName() throws Exception {
		new CompoundProperty("wrongName.getAttribute", TestSourceClass.class);
	}

	@Test
	public void movementAndRelatedNameAndProperty() throws Exception {
		TestSourceClass testData = new TestSourceClass();
		CompoundProperty compound = new CompoundProperty("attributeLink.getAttribute", testData.getClass());

		assertThat(compound.getMovementName(), is("attributeLink"));
		assertThat(compound.getRelatedName(), is("getAttribute"));
	}

	@Test
	public void movementAndRelatedNameAndProperty_onDoubleLink() throws Exception {
		TestDoubleSourceClass testData = new TestDoubleSourceClass();
		CompoundProperty compound = new CompoundProperty("doubleLink.attributeLink.getAttribute", testData.getClass());

		assertThat(compound.getMovementName(), is("doubleLink"));
		assertThat(compound.getRelatedName(), is("attributeLink.getAttribute"));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void setValueIllegal() throws Exception {
		TestSourceClass testObject = new TestSourceClass();

		Property compoundGetAttribute = new CompoundProperty("attributeLink.getAttribute", testObject.getClass());
		compoundGetAttribute.setValue("NEW", testObject);
	}
}
