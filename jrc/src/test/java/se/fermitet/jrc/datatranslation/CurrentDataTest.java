package se.fermitet.jrc.datatranslation;

import org.junit.Assert;
import org.junit.Test;
import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;
import se.fermitet.jrc.datatranslation.property.AttributeProperty;
import se.fermitet.jrc.datatranslation.property.CompoundProperty;
import se.fermitet.jrc.datatranslation.property.Property;
import se.fermitet.jrc.datatranslation.property.RelationProperty;
import se.fermitet.jrc.restparameter.ParameterData;
import se.fermitet.jrc.restparameter.SortParameter;

import java.util.*;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;

public class CurrentDataTest {
    @Test
    public void constructorAndGetter() {
        Object data = "data";

        assertThat(new CurrentData(data, null).getData(), is(data));
    }

    @Test
    public void isCollection() {
        assertThat(new CurrentData(Collections.emptyList(), null).isCollection(), is(true));
        assertThat(new CurrentData("String", null).isCollection(), is(false));
        assertThat(new CurrentData(null, null).isCollection(), is(false));
    }

    @Test
    public void isMap() {
        assertThat(new CurrentData(Collections.emptyMap(), null).isMap(), is(true));
        assertThat(new CurrentData("String", null).isMap(), is(false));
        assertThat(new CurrentData(null, null).isMap(), is(false));
    }

    @Test
    public void isEntity() {
        assertThat(new CurrentData(new EntityClass(), null).isEntity(), is(true));
        assertThat(new CurrentData("String", null).isEntity(), is(false));
        assertThat(new CurrentData(null, null).isEntity(), is(false));
    }

    @Test
    public void getSingleData_getClassOfSingleData_normal() {
        Object data = "data";

        CurrentData currentData = new CurrentData(data, null);

        assertThat(currentData.getSingleData(), is(data));
        assertEquals(data.getClass(), currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_null() {
        CurrentData currentData = new CurrentData(null, null);

        assertNull(currentData.getSingleData());
        assertNull(currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_collection() {
        Object singleData = "singleData";
        Collection data = Arrays.asList(singleData);

        CurrentData currentData = new CurrentData(data, null);

        assertThat(currentData.getSingleData(), is(singleData));
        assertEquals(singleData.getClass(), currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_collection_empty() {
        CurrentData currentData = new CurrentData(Collections.emptyList(), null);

        assertNull(currentData.getSingleData());
        assertNull(currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_collection_empty_withParameters() throws Exception {
        ParameterData parameters = new ParameterData();
        parameters.setAttributeNames(Arrays.asList("att"));
        parameters.setRelationNames(Arrays.asList("rel"));
        parameters.setSortParameters(Arrays.asList(new SortParameter("sortProperty")));

        CurrentData currentData = new CurrentData(Collections.emptyList(), parameters);

        assertNull(currentData.getSingleData());
        assertNull(currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_map() {
        Object singleData = "singleData";
        Map data = new HashMap();
        data.put("key", singleData);

        CurrentData currentData = new CurrentData(data, null);

        assertThat(currentData.getSingleData(), is(singleData));
        assertEquals(singleData.getClass(), currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_map_empty() {
        CurrentData currentData = new CurrentData(new HashMap<>(), null);

        assertNull(currentData.getSingleData());
        assertNull(currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_map_empty_withParameters() throws Exception {
        ParameterData parameters = new ParameterData();
        parameters.setAttributeNames(Arrays.asList("att"));
        parameters.setRelationNames(Arrays.asList("rel"));
        parameters.setSortParameters(Arrays.asList(new SortParameter("sortProperty")));

        CurrentData currentData = new CurrentData(Collections.emptyMap(), parameters);

        assertNull(currentData.getSingleData());
        assertNull(currentData.getClassOfSingleData());
    }

    @Test
    public void getSingleData_getClassOfSingleData_deeper() {
        Object singleData = "singleData";
        Collection data = Arrays.asList(Arrays.asList(singleData));

        CurrentData currentData = new CurrentData(data, null);

        assertThat(currentData.getSingleData(), is(singleData));
        assertEquals(singleData.getClass(), currentData.getClassOfSingleData());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDataAsCollection_null() {
        new CurrentData(null, null).getDataAsCollection();
    }

    @Test
    public void getDataAsCollection_collection() {
        Collection collection = Collections.emptyList();

        assertThat(new CurrentData(collection, null).getDataAsCollection(), is(collection));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDataAsCollection_nonCollection() {
        new CurrentData("test", null).getDataAsCollection();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDataAsMap_null() {
        new CurrentData(null, null).getDataAsMap();
    }

    @Test
    public void getDataAsMap_map() {
        Map map = new HashMap();

        assertThat(new CurrentData(map, null).getDataAsMap(), is(map));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDataAsMap_nonMap() {
        new CurrentData("test", null).getDataAsMap();
    }

    @Test
    public void getSelectionProperties_noDefaults() {
        EntityClass data = new EntityClass();
        ParameterData urlParameters = new ParameterData();
        urlParameters.setAttributeNames(Arrays.asList("att", "related.data"));
        urlParameters.setRelationNames(Arrays.asList("rel"));

        CurrentData currentData = new CurrentData(data, urlParameters);

        Collection<Property> selectionProperties = currentData.getSelectionProperties();
        assertThat(selectionProperties.size(), is(3));

        boolean attPropertyFound = false;
        boolean relPropertyFound = false;
        boolean compoundPropertyFound = false;

        for (Property prop : selectionProperties) {
            if (prop instanceof AttributeProperty) {
                assertThat(prop.getName(), is("att"));
                attPropertyFound = true;
            }
            if (prop instanceof RelationProperty) {
                assertThat(prop.getName(), is("rel"));
                relPropertyFound = true;
            }
            if (prop instanceof CompoundProperty) {
                assertThat(prop.getName(), is("related.data"));
                compoundPropertyFound = true;
            }
        }
        assertTrue(attPropertyFound);
        assertTrue(relPropertyFound);
        assertTrue(compoundPropertyFound);
    }

    @Test
    public void getSelectionProperties_defaults() throws Exception {
        EntityClass data = new EntityClass();
        ParameterData urlParameters = new ParameterData();
        urlParameters.setRelationNames(Arrays.asList("rel"));

        CurrentData currentData = new CurrentData(data, urlParameters);

        Collection<Property> selectionProperties = currentData.getSelectionProperties();
        assertThat(selectionProperties.size(), is(3));

        boolean attPropertyFound = false;
        boolean relPropertyFound = false;
        boolean relatedPropertyFound = false;

        for (Property prop : selectionProperties) {
            if (prop instanceof AttributeProperty) {
                if (prop.getName().equals("att")) attPropertyFound = true;
                if (prop.getName().equals("related")) relatedPropertyFound = true;
            }
            if (prop instanceof RelationProperty) {
                assertThat(prop.getName(), is("rel"));
                relPropertyFound = true;
            }
        }
        assertTrue(attPropertyFound);
        assertTrue(relPropertyFound);
        assertTrue(relatedPropertyFound);
    }

    @Test
    public void moveIntoCollection() throws Exception {
        EntityClass data1 = new EntityClass();
        EntityClass data2 = new EntityClass();

        ParameterData urlParameters = new ParameterData();
        urlParameters.setAttributeNames(Arrays.asList("att", "related.data"));
        urlParameters.setRelationNames(Arrays.asList("rel"));

        CurrentData currentData = new CurrentData(Arrays.asList(data1, data2), urlParameters);

        Collection<CurrentData> currentDatasFromCollection = currentData.moveIntoCollection();

        assertThat(currentDatasFromCollection.size(), is(2));

        // Should have data from inside the collection, but the same selection parameters
        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();
        CurrentData currentData1 = iter.next();
        assertThat(currentData1.getData(), is(data1));
        assertThat(currentData1.getSelectionProperties(), is(currentData.getSelectionProperties()));

        CurrentData currentData2 = iter.next();
        assertThat(currentData2.getData(), is(data2));
        assertThat(currentData2.getSelectionProperties(), is(currentData.getSelectionProperties()));
    }

    @Test
    public void moveIntoCollection_addsDefaults() throws Exception {
        String data1 = "Data as string";    // First one needs to be another class so that the set of default selection properties are different
        EntityClass data2 = new EntityClass();

        CurrentData currentData = new CurrentData(Arrays.asList(data1, data2), new ParameterData());

        Collection<CurrentData> currentDatasFromCollection = currentData.moveIntoCollection();

        assertThat(currentDatasFromCollection.size(), is(2));

        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();
        iter.next();
        CurrentData currentData2 = iter.next();

        assertThat(currentData2.getSelectionProperties().size(), is(greaterThan(0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void moveIntoCollection_notAtCollection() throws Exception {
        CurrentData currentData = new CurrentData("Some data that is not a collection", new ParameterData());

        currentData.moveIntoCollection();
    }

    @Test
    public void moveIntoCollection_sort_singleProperty() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.att = 1;

        EntityClass e2 = new EntityClass();
        e2.att = 2;

        EntityClass e3 = new EntityClass();
        e3.att = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("att")));

        CurrentData original = new CurrentData(Arrays.asList(e2, e3, e1), parameterData);

        Collection<CurrentData> currentDatasFromCollection = original.moveIntoCollection();

        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();

        CurrentData c1 = iter.next();
        CurrentData c2 = iter.next();
        CurrentData c3 = iter.next();

        assertThat(((EntityClass) c1.getData()).getAtt(), is(1));
        assertThat(((EntityClass) c2.getData()).getAtt(), is(2));
        assertThat(((EntityClass) c3.getData()).getAtt(), is(3));
    }

    @Test
    public void moveIntoCollection_sort_singleProperty_descending() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.att = 1;

        EntityClass e2 = new EntityClass();
        e2.att = 2;

        EntityClass e3 = new EntityClass();
        e3.att = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("-att")));

        CurrentData original = new CurrentData(Arrays.asList(e2, e3, e1), parameterData);

        Collection<CurrentData> currentDatasFromCollection = original.moveIntoCollection();

        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();

        CurrentData c1 = iter.next();
        CurrentData c2 = iter.next();
        CurrentData c3 = iter.next();

        assertThat(((EntityClass) c1.getData()).getAtt(), is(3));
        assertThat(((EntityClass) c2.getData()).getAtt(), is(2));
        assertThat(((EntityClass) c3.getData()).getAtt(), is(1));
    }

    @Test
    public void moveIntoCollection_sort_compoundProperty() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.getRelated().data = 1;

        EntityClass e2 = new EntityClass();
        e2.getRelated().data = 2;

        EntityClass e3 = new EntityClass();
        e3.getRelated().data = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("related.data")));

        CurrentData original = new CurrentData(Arrays.asList(e2, e3, e1), parameterData);

        Collection<CurrentData> currentDatasFromCollection = original.moveIntoCollection();

        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();

        CurrentData c1 = iter.next();
        CurrentData c2 = iter.next();
        CurrentData c3 = iter.next();

        assertThat(((EntityClass) c1.getData()).getRelated().getData(), is(1));
        assertThat(((EntityClass) c2.getData()).getRelated().getData(), is(2));
        assertThat(((EntityClass) c3.getData()).getRelated().getData(), is(3));
    }

    @Test
    public void moveIntoCollection_sort_multipleProperties() throws Exception {
        EntityClass e1 = new EntityClass();
        EntityClass e2 = new EntityClass();
        EntityClass e3 = new EntityClass();

        e1.att = 5;
        e2.att = 5;
        e3.att = 3;

        e1.rel = 2;
        e2.rel = 4;
        e3.rel = 4;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("att"), new SortParameter("rel")));

        CurrentData original = new CurrentData(Arrays.asList(e2, e3, e1), parameterData);

        Collection<CurrentData> currentDatasFromCollection = original.moveIntoCollection();

        Iterator<CurrentData> iter = currentDatasFromCollection.iterator();

        EntityClass c1 = (EntityClass) iter.next().getData();
        EntityClass c2 = (EntityClass) iter.next().getData();
        EntityClass c3 = (EntityClass) iter.next().getData();

        assertThat(c1.getAtt(), is(3));
        assertThat(c1.getRel(), is(4));

        assertThat(c2.getAtt(), is(5));
        assertThat(c2.getRel(), is(2));

        assertThat(c3.getAtt(), is(5));
        assertThat(c3.getRel(), is(4));
    }

    @Test
    public void moveIntoCollection_deeperSort() throws Exception {
        EntityClass data = new EntityClass();

        EntityWithContents c1 = new EntityWithContents();
        c1.contents = 1;

        RelatedEntityClass r = new RelatedEntityClass();
        r.relatedCollection.add(c1);

        data.collection = Arrays.asList(r);

        ParameterData parameterData = new ParameterData();
        parameterData.setRelationNames(Arrays.asList("collection", "collection.relatedCollection"));
        parameterData.setSortParameters(Arrays.asList(new SortParameter("collection.relatedCollection", "contents")));

        CurrentData original = new CurrentData(data, parameterData);
        CurrentData currentDataForCollection = original.moveIntoProperty(findSelectionPropertyByName(original, "collection"));


        Collection<CurrentData> currentDatasFromCollection = currentDataForCollection.moveIntoCollection();


        CurrentData oneCurrentDataFromCollection = currentDatasFromCollection.iterator().next();
        List<SortParameter> sortParameters = oneCurrentDataFromCollection.getSortParameters();

        assertThat(sortParameters.size(), is(1));

        SortParameter sortParameter = sortParameters.iterator().next();

        assertThat(sortParameter.getSortByName(), is("contents"));
        assertThat(sortParameter.getSortCollection(), is("relatedCollection"));
    }

    @Test
    public void moveIntoMap() throws Exception {
        EntityClass data1 = new EntityClass();
        EntityClass data2 = new EntityClass();

        ParameterData urlParameters = new ParameterData();
        urlParameters.setAttributeNames(Arrays.asList("att", "related.data"));
        urlParameters.setRelationNames(Arrays.asList("rel"));

        Map<String, EntityClass> dataMap = new HashMap<>();
        dataMap.put("d1", data1);
        dataMap.put("d2", data2);

        CurrentData currentData = new CurrentData(dataMap, urlParameters);

        Map<Object, CurrentData> currentDatasFromMap = currentData.moveIntoMap();

        assertThat(currentDatasFromMap.size(), is(2));

        // Should have data from inside the map, but the same selection parameters
        CurrentData currentData1 = currentDatasFromMap.get("d1");
        assertThat(currentData1.getData(), is(data1));
        assertThat(currentData1.getSelectionProperties(), is(currentData.getSelectionProperties()));

        CurrentData currentData2 = currentDatasFromMap.get("d2");
        assertThat(currentData2.getData(), is(data2));
        assertThat(currentData2.getSelectionProperties(), is(currentData.getSelectionProperties()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void moveIntoMap_notAtMap() throws Exception {
        CurrentData currentData = new CurrentData("Some data that is not a map", new ParameterData());

        currentData.moveIntoMap();
    }

    @Test
    public void moveIntoMap_sort_singleProperty() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.att = 1;

        EntityClass e2 = new EntityClass();
        e2.att = 2;

        EntityClass e3 = new EntityClass();
        e3.att = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("att")));

        Map<Object, Object> data = new LinkedHashMap<>();
        data.put("1", e2);
        data.put("2", e3);
        data.put("3", e1);

        CurrentData original = new CurrentData(data, parameterData);

        Map<Object, CurrentData> currentDatasFromMap = original.moveIntoMap();

        Iterator<Object> iter = currentDatasFromMap.keySet().iterator();

        EntityClass fromResult1 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult2 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult3 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();

        assertThat(fromResult1.getAtt(), is(1));
        assertThat(fromResult2.getAtt(), is(2));
        assertThat(fromResult3.getAtt(), is(3));
    }

    @Test
    public void moveIntoMap_sort_singleProperty_descending() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.att = 1;

        EntityClass e2 = new EntityClass();
        e2.att = 2;

        EntityClass e3 = new EntityClass();
        e3.att = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("-att")));

        Map<Object, Object> data = new LinkedHashMap<>();
        data.put("1", e2);
        data.put("2", e3);
        data.put("3", e1);

        CurrentData original = new CurrentData(data, parameterData);

        Map<Object, CurrentData> currentDatasFromMap = original.moveIntoMap();

        Iterator<Object> iter = currentDatasFromMap.keySet().iterator();

        EntityClass fromResult1 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult2 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult3 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();

        assertThat(fromResult1.getAtt(), is(3));
        assertThat(fromResult2.getAtt(), is(2));
        assertThat(fromResult3.getAtt(), is(1));
    }

    @Test
    public void moveIntoMap_sort_compoundProperty() throws Exception {
        EntityClass e1 = new EntityClass();
        e1.getRelated().data = 1;

        EntityClass e2 = new EntityClass();
        e2.getRelated().data = 2;

        EntityClass e3 = new EntityClass();
        e3.getRelated().data = 3;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("related.data")));

        Map<Object, Object> data = new LinkedHashMap<>();
        data.put("1", e2);
        data.put("2", e3);
        data.put("3", e1);

        CurrentData original = new CurrentData(data, parameterData);

        Map<Object, CurrentData> currentDatasFromMap = original.moveIntoMap();

        Iterator<Object> iter = currentDatasFromMap.keySet().iterator();

        EntityClass fromResult1 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult2 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult3 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();

        assertThat(fromResult1.getRelated().getData(), is(1));
        assertThat(fromResult2.getRelated().getData(), is(2));
        assertThat(fromResult3.getRelated().getData(), is(3));
    }

    @Test
    public void moveIntoMap_sort_multipleProperties() throws Exception {
        EntityClass e1 = new EntityClass();
        EntityClass e2 = new EntityClass();
        EntityClass e3 = new EntityClass();

        e1.att = 5;
        e2.att = 5;
        e3.att = 3;

        e1.rel = 2;
        e2.rel = 4;
        e3.rel = 4;

        ParameterData parameterData = new ParameterData();
        parameterData.setSortParameters(Arrays.asList(new SortParameter("att"), new SortParameter("rel")));

        Map<Object, Object> data = new LinkedHashMap<>();
        data.put("1", e2);
        data.put("2", e3);
        data.put("3", e1);

        CurrentData original = new CurrentData(data, parameterData);

        Map<Object, CurrentData> currentDatasFromMap = original.moveIntoMap();

        Iterator<Object> iter = currentDatasFromMap.keySet().iterator();

        EntityClass fromResult1 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult2 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();
        EntityClass fromResult3 = (EntityClass) currentDatasFromMap.get(iter.next()).getData();

        assertThat(fromResult1.getAtt(), is(3));
        assertThat(fromResult1.getRel(), is(4));

        assertThat(fromResult2.getAtt(), is(5));
        assertThat(fromResult2.getRel(), is(2));

        assertThat(fromResult3.getAtt(), is(5));
        assertThat(fromResult3.getRel(), is(4));
    }

    @Test
    public void moveIntoProperty_withoutDefaults() throws Exception {
        EntityClass data = new EntityClass();
        data.att = 3;

        ParameterData urlParameters = new ParameterData();
        urlParameters.setAttributeNames(Arrays.asList("att", "related", "related.data"));
        urlParameters.setRelationNames(Arrays.asList("rel"));

        CurrentData topLevelCurrentData = new CurrentData(data, urlParameters);

        CurrentData attCurrentData = topLevelCurrentData.moveIntoProperty(findSelectionPropertyByName(topLevelCurrentData, "att"));
        assertThat(attCurrentData.getData(), is(data.getAtt()));
        assertThat(attCurrentData.getSelectionProperties().size(), is(0));

        CurrentData relatedCurrentData = topLevelCurrentData.moveIntoProperty(findSelectionPropertyByName(topLevelCurrentData, "related"));
        assertThat(relatedCurrentData.getData(), is(data.getRelated()));
        assertThat(relatedCurrentData.getSelectionProperties().size(), is(1));

        Property selectionPropertyInRelatedData = relatedCurrentData.getSelectionProperties().iterator().next();
        assertThat(selectionPropertyInRelatedData.getName(), is("data"));
        assertTrue(selectionPropertyInRelatedData instanceof AttributeProperty);
    }

    @Test
    public void moveIntoProperty_withDefaults() throws Exception {
        EntityClass data = new EntityClass();
        data.att = 3;

        CurrentData topLevelCurrentData = new CurrentData(data, new ParameterData());

        CurrentData attCurrentData = topLevelCurrentData.moveIntoProperty(findSelectionPropertyByName(topLevelCurrentData, "att"));
        assertThat(attCurrentData.getData(), is(data.getAtt()));
        assertThat(attCurrentData.getSelectionProperties().size(), is(0));

        CurrentData relatedCurrentData = topLevelCurrentData.moveIntoProperty(findSelectionPropertyByName(topLevelCurrentData, "related"));
        assertThat(relatedCurrentData.getData(), is(data.getRelated()));
        assertThat(relatedCurrentData.getSelectionProperties().size(), is(1));

        Property selectionPropertyInRelatedData = relatedCurrentData.getSelectionProperties().iterator().next();
        assertThat(selectionPropertyInRelatedData.getName(), is("data"));
        assertTrue(selectionPropertyInRelatedData instanceof AttributeProperty);
    }

    @Test(expected = IllegalArgumentException.class)
    public void moveIntoProperty_nonexsistentProperty() throws Exception {
        EntityClass data = new EntityClass();

        CurrentData currentData = new CurrentData(data, new ParameterData());

        currentData.moveIntoProperty(new AttributeProperty("data", RelatedEntityClass.class));
    }

    @Test
    public void moveIntoProperty_stripsUnrelatedSortParameters() {
        SortParameter propertySortParameter = new SortParameter("att");
        SortParameter collectionSortParameter = new SortParameter("collection", "data");

        ParameterData urlParameters = new ParameterData();
        urlParameters.setSortParameters(Arrays.asList(propertySortParameter, collectionSortParameter));
        urlParameters.setRelationNames(Arrays.asList("rel"));

        CurrentData currentData = new CurrentData(new EntityClass(), urlParameters);

        CurrentData newCurrentData = currentData.moveIntoProperty(findSelectionPropertyByName(currentData, "rel"));

        List<SortParameter> newSortParameters = newCurrentData.getSortParameters();

        assertThat(newSortParameters.isEmpty(), is(true));
    }

    @Test
    public void moveIntoProperty_stripsSortParametersOfDeeperCollections() {
        SortParameter deeperSortParameter = new SortParameter("related.relatedCollection", "contents");

        ParameterData urlParameters = new ParameterData();
        urlParameters.setSortParameters(Arrays.asList(deeperSortParameter));
        urlParameters.setRelationNames(Arrays.asList("related.relatedCollection"));

        CurrentData currentData = new CurrentData(new EntityClass(), urlParameters);

        CurrentData newCurrentData = currentData.moveIntoProperty(findSelectionPropertyByName(currentData, "related"));

        List<SortParameter> newSortParameters = newCurrentData.getSortParameters();

        assertThat(newSortParameters.size(), is(1));

        SortParameter newSortParameter = newSortParameters.iterator().next();

        assertThat(newSortParameter.getSortCollection(), is("relatedCollection"));
        assertThat(newSortParameter.getSortByName(), is("contents"));
    }

    private Property findSelectionPropertyByName(CurrentData currentData, String name) {
        return currentData.getSelectionProperties().stream()
                .filter(prop -> prop.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Test
    public void getSortParameters() {
        List<SortParameter> sortParameters = Arrays.asList(new SortParameter("sortByProperty"));

        ParameterData URLParameters = new ParameterData();
        URLParameters.setSortParameters(sortParameters);

        CurrentData currentData = new CurrentData("data insignificant for this test", URLParameters);

        assertThat(currentData.getSortParameters(), is(sortParameters));
    }

    @Test
    public void getSortParameters_noUrlParameters() {
        List<SortParameter> collection = new CurrentData("data", null).getSortParameters();
        assertThat(collection.isEmpty(), is(true));

    }

    @Entity
    private class EntityClass {
        @Attribute int att;
        int getAtt() { return this.att; }

        @Attribute RelatedEntityClass related = new RelatedEntityClass();
        RelatedEntityClass getRelated() { return this.related; }

        @Relation int rel;
        int getRel() { return this.rel; }

        @Relation Collection<RelatedEntityClass> collection = new ArrayList<>();
        Collection<RelatedEntityClass> getCollection() { return collection; }
    }

    @Entity
    private class RelatedEntityClass {
        @Attribute int data;
        int getData() { return this.data; }

        @Relation Collection<EntityWithContents> relatedCollection = new ArrayList<>();
        Collection<EntityWithContents> getRelatedCollection() { return relatedCollection; }
    }

    @Entity
    private class EntityWithContents {
        @Attribute int contents;
        int getContents() { return contents; }
    }


}