package se.fermitet.jrc.datatranslation.property;

import org.junit.Test;
import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PropertyTest {


	@Test
	public void getAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "getAttribute", true, testObject.getGetAttribute(), true);
	}

	@Test
	public void isAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "isAttribute", true, testObject.isIsAttribute(), true);
	}

	@Test
	public void hasAttribute() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "hasAttribute", true, testObject.hasHasAttribute(), true);
	}

	@Test
	public void relation() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "relation", false, testObject.getRelation(), true);
	}

	@Test
	public void attributeIsInSuperclass() throws Exception {
		TestSubclass testObject = new TestSubclass();
		testNonCompoundProperty(testObject, "getAttribute", true, testObject.getGetAttribute(), true);
	}

	@Test
	public void attributeOnGetter() throws Exception {
		TestClass testObject = new TestClass();

		testNonCompoundProperty(testObject, "attributeOnGetter", true, testObject.getAttributeOnGetter(), false);
	}

	@Test
	public void derivedAttributeOverridenWithoutAnnotation() throws Exception {
		TestSubclass testObject = new TestSubclass();
		testNonCompoundProperty(testObject, "attributeOnGetterWhereSubclassOverridesWithoutAnnotation", true, "overridden value", false);
	}

	@Test
	public void derivedRelationOverriddenWitoutAnnotation() throws Exception {
		TestSubclass testObject = new TestSubclass();
		testNonCompoundProperty(testObject, "relationOnGetterWhereSubclassOverridesWithoutAnnotation", false, "overridden value", false);
	}

	private void testNonCompoundProperty(TestClass testObject, String propName, boolean isAttribute, String expectedValue, boolean testSetter) throws Exception {
		Property property = null;
		if (isAttribute)
			property = new AttributeProperty(propName, testObject.getClass());
		else
			property = new se.fermitet.jrc.datatranslation.property.RelationProperty(propName, testObject.getClass());

		assertThat(property.getName(), is(propName));
		assertThat(property.getValue(testObject), is(expectedValue));
		assertFalse(property instanceof se.fermitet.jrc.datatranslation.property.CompoundProperty);
		
		if(! testSetter) return;
		String newValue = "NEW";
		property.setValue(newValue, testObject);
		assertThat(property.getValue(testObject), is(newValue));
	}

	@Test(expected = IllegalArgumentException.class)
	public void illegalName() throws Exception {
		new AttributeProperty("ILLEGAL NAME", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void nonAttributeOrRelation() throws Exception {
		new se.fermitet.jrc.datatranslation.property.RelationProperty("nonAttributeOrRelation", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void attributeOnRelation() throws Exception {
		new AttributeProperty("relation", TestClass.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void relationOnattribute() throws Exception {
		new se.fermitet.jrc.datatranslation.property.RelationProperty("getAttribute", TestClass.class);
	}

	@Test
	public void defaultProperties() throws Exception {
		Collection<Property> defaults = se.fermitet.jrc.datatranslation.property.Property.getDefaultProperties(TestClass.class);

		assertThat(defaults.size(), is(5));

		Collection<String> propertyNames = defaults.stream().map(p -> p.getName()).collect(Collectors.toList());

		assertTrue(propertyNames.contains("getAttribute"));
		assertTrue(propertyNames.contains("isAttribute"));
		assertTrue(propertyNames.contains("hasAttribute"));
		assertTrue(propertyNames.contains("attributeOnGetter"));
		assertTrue(propertyNames.contains("attributeOnGetterWhereSubclassOverridesWithoutAnnotation"));
	}

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void callingWrongClass_get() throws Exception {
		TestClass testData = new TestClass();
		Property prop = new AttributeProperty("getAttribute", testData.getClass());

		Object value = prop.getValue(new Integer(23));	// Should be a string to be correct
	}

	@Test(expected = IllegalArgumentException.class)
	public void callingWrongClass_set() throws Exception {
		TestClass testData = new TestClass();
		Property prop = new AttributeProperty("getAttribute", testData.getClass());

		prop.setValue("NEW", new Integer(23));	// Should be a string to be correct
	}

	@Test
	public void getPropertyByName() throws Exception {
		se.fermitet.jrc.datatranslation.property.Property result = se.fermitet.jrc.datatranslation.property.Property.getByName("getAttribute", TestClass.class);
		assertThat(result.getName(), is("getAttribute"));
		assertTrue(result instanceof AttributeProperty);

		result = se.fermitet.jrc.datatranslation.property.Property.getByName("relation", TestClass.class);
		assertThat(result.getName(), is("relation"));
		assertTrue(result instanceof se.fermitet.jrc.datatranslation.property.RelationProperty);

		result = se.fermitet.jrc.datatranslation.property.Property.getByName("attributeLink.getAttribute", TestSourceClass.class);
		assertThat(result.getName(), is("attributeLink.getAttribute"));
		assertTrue(result instanceof se.fermitet.jrc.datatranslation.property.CompoundProperty);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getPropertyByName_wrongName() throws Exception {
		se.fermitet.jrc.datatranslation.property.Property.getByName("ILLEGAL NAME", TestClass.class);
	}


	@Entity
	static class TestClass {
		@Attribute private String getAttribute = "getAttribute";
		@Attribute private String isAttribute = "isAttribute";
		@Attribute private String hasAttribute = "hasAttribute";
		@SuppressWarnings("unused")
		private String nonAttributeOrRelation;

		@Relation private String relation = "relation";

		public String getGetAttribute() {
			return getAttribute;
		}
		
		public void setGetAttribute(String getAttribute) {
			this.getAttribute = getAttribute;
		}

		public String isIsAttribute() {
			return isAttribute;
		}
		
		public String getIsAttribute() {
			return isAttribute;
		}

		public void setIsAttribute(String isAttribute) {
			this.isAttribute = isAttribute;
		}

		public String hasHasAttribute() {
			return hasAttribute;
		}
		
		public String getHasAttribute() {
			return hasAttribute;
		}

		public void setHasAttribute(String hasAttribute) {
			this.hasAttribute = hasAttribute;
		}

		@Attribute
		public String getAttributeOnGetter() {
			return "attributeOnGetter";
		}

		@Attribute
		public String getAttributeOnGetterWhereSubclassOverridesWithoutAnnotation() { return "attributeOnGetterWhereSubclassOverrides"; }

		@Relation public String getRelationOnGetterWhereSubclassOverridesWithoutAnnotation() { return "relationOnGetterWhereSubclassOverrides"; }

		public String getRelation() {
			return relation;
		}

		public void setRelation(String relation) {
			this.relation = relation;
		}
	}

	class TestSubclass extends TestClass {
		@Override
		public String getAttributeOnGetterWhereSubclassOverridesWithoutAnnotation() {
			return "overridden value";
		}

		@Override
		public String getRelationOnGetterWhereSubclassOverridesWithoutAnnotation() {
			return "overridden value";
		}
	}

	static class TestSourceClass {
		@Attribute TestClass attributeLink = new TestClass();
		public TestClass getAttributeLink() {
			return attributeLink;
		}
	}

	static class TestDoubleSourceClass {
		@Attribute private TestSourceClass doubleLink = new TestSourceClass();
		public TestSourceClass getDoubleLink() {
			return doubleLink;
		}
	}
}
