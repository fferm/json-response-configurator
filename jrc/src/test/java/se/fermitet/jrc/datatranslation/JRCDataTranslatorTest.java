package se.fermitet.jrc.datatranslation;

import org.junit.Before;
import org.junit.Test;
import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;
import se.fermitet.jrc.restparameter.ParameterData;
import se.fermitet.jrc.restparameter.SortParameter;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.Assert.*;

@SuppressWarnings({ "unchecked", "unused" })
public class JRCDataTranslatorTest {

	private JRCDataTranslator translator;

	@Before
	public void setUp() throws Exception {
		translator = new JRCDataTranslator();
	}

	@Test
	public void noAnnotations() throws Exception {
		class TestObject {}
		class NonAnnotatedClass {
			public String getString() { return "STRING"; }
			public int getInt() { return -1; }
			public Object getObject() { return new TestObject(); }
		}

		NonAnnotatedClass data = new NonAnnotatedClass();

		Object result = translator.createJson(data, getParameters(null, null, null));

		assertThat(result, sameInstance(data));
	}

	@Test
	public void emptyEntity() throws Exception {
		@Entity
		class EmptyEntity {
			private String aField = "HEJ";
			public String getAField() { return aField; }
		}

		EmptyEntity data = new EmptyEntity();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertTrue(result.isEmpty());
	}

	@Test
	public void nullValues() throws Exception {
		@Entity
		class EntityWithNullvalues {
			@Attribute String stringAtt = null;
			@Relation String stringRelation = null;
			public String getStringAtt() { return stringAtt; }
			public String getStringRelation() { return stringRelation; }
		}

		EntityWithNullvalues data = new EntityWithNullvalues();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("stringAtt"), Arrays.asList("stringRelation"), null));

		assertThat("Size of result", result.size(), is(2));
		assertNull(result.get("stringAtt"));
		assertNull(result.get("ststringRelationringAtt"));
	}

	@Test
	public void nullInput() throws Exception {
		Object result = translator.createJson(null, getParameters(null, null, null));
		assertNull(result);
	}

	@Test
	public void emptyCollectionInput() throws Exception {
		Object result = translator.createJson(Collections.emptyList(), getParameters(null, null, null));

		assertNull(result);
	}

	@Test
	public void emptyCollectionWithParameters() throws Exception {
		Object result = translator.createJson(Collections.emptyList(), getParameters(Arrays.asList("id"), Arrays.asList("someOther"), Arrays.asList(srp("sorting"))));

		assertNull(result);
	}

	@Test
	public void collectionOfEmptyCollectionInput() throws Exception {
		Object result = translator.createJson(Arrays.asList(Collections.emptyList()), getParameters(null, null, null));

		assertNull(result);
	}

	@Test
	public void emptyMapInput() throws Exception {
		Object result = translator.createJson(Collections.emptyMap(), getParameters(null, null, null));

		assertNull(result);
	}

	@Test
	public void emptyMapWithParameters() throws Exception {
		Object result = translator.createJson(Collections.emptyMap(), getParameters(Arrays.asList("id"), Arrays.asList("someOther"), Arrays.asList(srp("sorting"))));

		assertNull(result);
	}

	@Test
	public void mapOfEmptyMapInput() throws Exception {
		Map<Object, Object> data = new LinkedHashMap<>();
		data.put("empty", Collections.emptyMap());
		Object result = translator.createJson(data, getParameters(null, null, null));

		assertNull(result);
	}

	@Test
	public void attribute_primitives() throws Exception {
		@Entity
		class EntityWithPrimitiveAttributes {
			@Attribute private int intAttribute = 5;
			public int getIntAttribute() { return intAttribute;	}

			@Attribute private boolean attributeWithIsName = true;
			public boolean isAttributeWithIsName() { return attributeWithIsName; }

			@Attribute private boolean attributeWithHasName = true;
			public boolean hasAttributeWithHasName() { return attributeWithHasName; }
		}

		EntityWithPrimitiveAttributes data = new EntityWithPrimitiveAttributes();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(3));
		assertThat("getter with get-name", result.get("intAttribute"), is(data.getIntAttribute()));
		assertThat("getter with is-name", result.get("attributeWithIsName"), is(data.isAttributeWithIsName()));
		assertThat("getter with has-name", result.get("attributeWithHasName"), is(data.hasAttributeWithHasName()));
	}

	@Test
	public void attribute_object_notEntity() throws Exception {
		class NonEntity {};

		@Entity
		class EntityWithNonEntityAttribute {
			@Attribute private NonEntity nonEntityAttribute = new NonEntity();
			public NonEntity getNonEntityAttribute() { return nonEntityAttribute; }
		}

		EntityWithNonEntityAttribute data = new EntityWithNonEntityAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));
		assertThat(result.get("nonEntityAttribute"), sameInstance(data.getNonEntityAttribute()));
	}

	@Test
	public void attribute_objects_entities() throws Exception {
		@Entity
		class AttributeObjectWhichIsEmpty {}

		@Entity
		class EntityWithEntityAttribute {
			@Attribute private AttributeObjectWhichIsEmpty entityAttribute = new AttributeObjectWhichIsEmpty();
			public AttributeObjectWhichIsEmpty getEntityAttribute() { return entityAttribute; }
		}

		EntityWithEntityAttribute data = new EntityWithEntityAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));

		Map<String, Object> attributeMap = (Map<String, Object>) result.get("entityAttribute");
		assertThat("Size of attributeMap", attributeMap.size(), is(0));
	}

	@Test
	public void attribute_collections_primitives() throws Exception {
		@Entity
		class EntityWithCollectionAttribute {
			@Attribute Collection<Integer> intCollection = Arrays.asList(1,2,3,4,5,6);
			public Collection<Integer> getIntCollection() {	return intCollection; }

		}

		EntityWithCollectionAttribute data = new EntityWithCollectionAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));

		Collection<Integer> collectionFromResult = (Collection<Integer>) result.get("intCollection");

		assertArrayEquals(new Object[]{1,2,3,4,5,6}, collectionFromResult.toArray());
	}

	@Test
	public void relation_collections_entities_mixed_with_other_object() throws Exception {
		@Entity
		class EntityValue {
			@Attribute private int value;
			public int getValue() { return value; }
			public EntityValue(int value) {
				super();
				this.value = value;
			}
		}
		@Entity
		class EntityWithCollectionAttribute {
			@Attribute Collection<Object> collection = Arrays.asList(new EntityValue(23), new EntityValue(35), "Hej");
			public Collection<Object> getCollection() {	return collection; }
		}

		EntityWithCollectionAttribute data = new EntityWithCollectionAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));

		Collection<Object> collectionFromResult = (Collection<Object>) result.get("collection");

		assertThat("Size of collection", collectionFromResult.size(), is(3));

		Iterator<Object> iterator = collectionFromResult.iterator();

		Map<String, Object> firstElementInCollection = (Map<String, Object>) iterator.next();
		assertThat("Size of first element", firstElementInCollection.size(), is(1));
		assertThat("FirstElement value", firstElementInCollection.get("value"), is(23));

		Map<String, Object> secondElementInCollection = (Map<String, Object>) iterator.next();
		assertThat("Size of second element", secondElementInCollection.size(), is(1));
		assertThat("SecondElement value", secondElementInCollection.get("value"), is(35));

		assertThat("ThirdElement", iterator.next(), is("Hej"));
	}

	@Test
	public void collectionOfObjects() throws Exception {
		@Entity
		class EntityValue {
			@Attribute private int value;
			public int getValue() { return value; }
			public EntityValue(int value) {
				super();
				this.value = value;
			}
		}

		Collection<Object> allData = Arrays.asList("A STRING", new EntityValue(3));

		Collection<Object> result =  (Collection<Object>) translator.createJson(allData, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(2));

		Iterator<Object> iter = result.iterator();

		String firstObject = (String) iter.next();
		assertThat(firstObject, is("A STRING"));

		Map<String, Object> secondObject = (Map<String, Object>) iter.next();
		assertThat("Size of second object", secondObject.size(), is(1));
		assertThat(secondObject.get("value"), is(3));
	}

	@Test
	public void specificAttributes() throws Exception {
		@Entity
		class EntityWithPrimitiveAttributes {
			@Attribute private String firstAttribute = "FirstAttribute";
			public String getFirstAttribute() { return firstAttribute;	}

			@Attribute private String secondAttribute = "SecondAttribute";
			public String getSecondAttribute() { return secondAttribute;	}

			@Attribute private String thirdAttribute = "ThirdAttribute";
			public String getThirdAttribute() { return thirdAttribute;	}
		}

		EntityWithPrimitiveAttributes data = new EntityWithPrimitiveAttributes();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("firstAttribute", "secondAttribute"), null, null));

		assertThat("Size of result", result.size(), is(2));
		assertThat("firstAttribute", result.get("firstAttribute"), is(data.getFirstAttribute()));
		assertThat("secondAttribute", result.get("secondAttribute"), is(data.getSecondAttribute()));
	}

	@Test(expected=IllegalArgumentException.class)
	public void specificAttributes_wrongName() throws Exception {
		@Entity
		class EntityWithPrimitiveAttributes {
			@Attribute private String firstAttribute = "FirstAttribute";
			public String getFirstAttribute() { return firstAttribute;	}
		}

		EntityWithPrimitiveAttributes data = new EntityWithPrimitiveAttributes();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("wrongName"), null, null));
	}

	@Test(expected=IllegalArgumentException.class)
	public void specificAttributes_nonAttribute() throws Exception {
		@Entity
		class EntityWithPrimitiveAttributes {
			private String firstAttribute = "FirstAttribute";
			public String getFirstAttribute() { return firstAttribute;	}
		}

		EntityWithPrimitiveAttributes data = new EntityWithPrimitiveAttributes();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("firstAttribute"), null, null));
	}

	@Test
	public void relations_simple_objects_entities() throws Exception {
		@Entity
		class RelationObject {
			@Attribute private String first = "First";
			public String getFirst() { return first;	}

			@Attribute private String second = "Second";
			public String getSecond() { return second;	}
		}

		@Entity
		class EntityWithEntityRelation {
			@Relation private RelationObject entityRelation = new RelationObject();
			public RelationObject getEntityRelation() { return entityRelation; }
		}

		EntityWithEntityRelation data = new EntityWithEntityRelation();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("entityRelation"), null));

		assertThat("Size of result", result.size(), is(1));

		Map<String, Object> relationMap = (Map<String, Object>) result.get("entityRelation");
		assertThat("Size of relationMap", relationMap.size(), is(2));
		assertThat(relationMap.get("first"), is("First"));
		assertThat(relationMap.get("second"), is("Second"));
	}

	@Test
	public void default_allAttributes_no_relations_using_null() throws Exception {
		@Entity
		class RelationObjectWhichIsEmpty {}

		@Entity
		class EntityWithOneRelationAndOneAttribute {
			@Relation private RelationObjectWhichIsEmpty relation = new RelationObjectWhichIsEmpty();
			public RelationObjectWhichIsEmpty getRelation() { return relation; }

			@Attribute private String attribute = "ATTRIBUTE";
			public String getAttribute() { return attribute; }
		}

		EntityWithOneRelationAndOneAttribute data = new EntityWithOneRelationAndOneAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));

		assertThat(result.get("attribute"), is(data.getAttribute()));
	}

	@Test
	public void default_allAttributes_no_relations_using_emptyList() throws Exception {
		@Entity
		class RelationObjectWhichIsEmpty {}

		@Entity
		class EntityWithOneRelationAndOneAttribute {
			@Relation private RelationObjectWhichIsEmpty relation = new RelationObjectWhichIsEmpty();
			public RelationObjectWhichIsEmpty getRelation() { return relation; }

			@Attribute private String attribute = "ATTRIBUTE";
			public String getAttribute() { return attribute; }
		}

		EntityWithOneRelationAndOneAttribute data = new EntityWithOneRelationAndOneAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(new ArrayList<String>(), new ArrayList<String>(), null));

		assertThat("Size of result", result.size(), is(1));
		assertThat(result.get("attribute"), is(data.getAttribute()));
	}

	@Test
	public void specificRelations() throws Exception {
		@Entity
		class EntityWithPrimitiveRelations {
			@Relation private String firstRelation = "First";
			public String getFirstRelation() { return firstRelation;	}

			@Relation private String secondRelation = "Second";
			public String getSecondRelation() { return secondRelation;	}

			@Relation private String thirdRelation = "Third";
			public String getThirdRelation() { return thirdRelation;	}
		}

		EntityWithPrimitiveRelations data = new EntityWithPrimitiveRelations();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("firstRelation", "secondRelation"), null));

		assertThat("Size of result", result.size(), is(2));
		assertThat("first", result.get("firstRelation"), is(data.getFirstRelation()));
		assertThat("second", result.get("secondRelation"), is(data.getSecondRelation()));
	}

	@Test(expected=IllegalArgumentException.class)
	public void specificRelations_wrongName() throws Exception {
		@Entity
		class EntityWithPrimitiveRelation {
			@Relation private String first = "First";
			public String getFirst() { return first;	}
		}

		EntityWithPrimitiveRelation data = new EntityWithPrimitiveRelation();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("wrongName"), null));
	}

	@Test(expected=IllegalArgumentException.class)
	public void specificRelations_nonRelation() throws Exception {
		@Entity
		class EntityWithPrimitiveAttribute {
			private String first = "First";
			public String getFirst() { return first;	}
		}

		EntityWithPrimitiveAttribute data = new EntityWithPrimitiveAttribute();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("first"), null));
	}

	@Test
	public void specificRelations_includeAllAttributes() throws Exception {
		@Entity
		class EntityWithPrimitiveRelations {
			@Relation private String firstRelation = "First";
			public String getFirstRelation() { return firstRelation;	}

			@Relation private String second = "Second";
			public String getSecond() { return second;	}

			@Attribute private String third = "Third";
			public String getThird() { return third;	}
		}

		EntityWithPrimitiveRelations data = new EntityWithPrimitiveRelations();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("firstRelation"), null));

		assertThat("Size of result", result.size(), is(2));
		assertThat("first", result.get("firstRelation"), is(data.getFirstRelation()));
		assertThat("second", result.get("third"), is(data.getThird()));
	}

	@Test
	public void attributesOnRelatedObjectInCollection() throws Exception {
		@Entity
		class EntitySecondary {
			@Attribute private String value;
			public String getValue() { return value;	}
			@Attribute private String other;
			public String getOther() { return other; }
			public EntitySecondary(String value, String other) {
				super();
				this.value = value;
				this.other = other;
			}
		}
		@Entity
		class EntityPrimary {
			@Relation private Collection<EntitySecondary> relation = Arrays.asList(new EntitySecondary("One", "Two"), new EntitySecondary("Three", "Four"));
			public Collection<EntitySecondary> getRelation() { return relation;	}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("relation.value"), Arrays.asList("relation"), null));

		assertThat("Size of result", result.size(), is(1));

		Collection<Map<String, Object>> rel = (Collection<Map<String, Object>>) result.get("relation");

		assertThat(rel.size(), is(2));

		Iterator<Map<String, Object>> iter = rel.iterator();
		Map<String, Object> sec1 = iter.next();
		Map<String, Object> sec2 = iter.next();

		assertThat(sec1.size(), is(1));
		assertThat(sec2.size(), is(1));

		assertThat(sec1.get("value"), is("One"));
		assertThat(sec2.get("value"), is("Three"));
	}

	@Test
	public void attributesOnRelatedObjectInEmptyCollection() throws Exception {
		@Entity
		class EntitySecondary {
			@Attribute private String value;
			public String getValue() { return value;	}
			@Attribute private String other;
			public String getOther() { return other; }
			public EntitySecondary(String value, String other) {
				super();
				this.value = value;
				this.other = other;
			}
		}
		@Entity
		class EntityPrimary {
			@Relation private Collection<EntitySecondary> relation = new ArrayList<EntitySecondary>();
			public Collection<EntitySecondary> getRelation() { return relation;	}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("relation.value"), Arrays.asList("relation"), null));

		assertThat("Size of result", result.size(), is(1));

		Collection<Map<String, Object>> rel = (Collection<Map<String, Object>>) result.get("relation");

		assertThat(rel.size(), is(0));
	}

	@Test
	public void attributesOnRelatedObject() throws Exception {
		@Entity
		class DestinationObject {}
		@Entity
		class SourceObject {
			@Relation private DestinationObject destObject = new DestinationObject();
			public DestinationObject getDestObject() { return destObject; }

			@Attribute private String str = "hello";
			public String getStr() { return str; }
		}

		SourceObject data = new SourceObject();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("str"), Arrays.asList("destObject"), null));

		assertThat("Size of result", result.size(), is(2));
	}

	@Test
	public void attributesOnCollectionOfRelatedObject() throws Exception {
		@Entity
		class EntitySecondary {
			@Attribute private String value;
			public String getValue() { return value;	}
			public EntitySecondary(String value) {this.value = value;}
		}

		@Entity
		class EntityPrimary {
			@Relation private Collection<EntitySecondary> relation = Arrays.asList(new EntitySecondary("first"), new EntitySecondary("second"));
			public Collection<EntitySecondary> getRelation() { return relation;	}
			@Attribute private String att = "Att";
			public String getAtt() {return att;}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("att"), Arrays.asList("relation"), null));

		assertThat("Size of result", result.size(), is(2));

		Collection<Object> related = (Collection<Object>) result.get("relation");
		assertThat("Size of other object", related.size(), is(2));

		Map<String, Object> firstRelated = (Map<String, Object>) related.iterator().next();
		assertThat(firstRelated.size(), is(1));
		assertThat(firstRelated.get("value"), is("first"));
	}

	@Test
	public void onlyAttributesOnRelatedShouldProvideDefaultOnPrimary() throws Exception {
		@Entity
		class EntitySecondary {
			@Attribute private String value = "value";
			public String getValue() { return value;	}
		}

		@Entity
		class EntityPrimary {
			@Relation private EntitySecondary relation = new EntitySecondary();
			public EntitySecondary getRelation() { return relation;	}
			@Attribute private String att = "Att";
			public String getAtt() {return att;}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("relation.value"), Arrays.asList("relation"), null));

		assertThat("Size of result", result.size(), is(2));
		assertThat(result.get("att"), is(data.getAtt()));
	}

	@Test(expected=IllegalArgumentException.class)
	public void attributesOnRelatedObject_withoutRelation() throws Exception {
		@Entity
		class EntitySecondary {
			@Attribute private String first = "First";
			public String getFirst() { return first;	}

			@Attribute private String second = "Second";
			public String getSecond() { return second;	}
		}

		@Entity
		class EntityPrimary {
			@Relation private EntitySecondary relation = new EntitySecondary();
			public EntitySecondary getRelation() { return relation;	}
			@Attribute private String string = "Hej";
			public String getString() { return string; };
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("id", "relation.first"), new ArrayList<String>(), null));
	}

	@Test
	public void doubleRelations() throws Exception {
		@Entity
		class EntityTertiary {
			@Attribute private String first = "First";
			public String getFirst() { return first;	}

			@Attribute private String second = "Second";
			public String getSecond() { return second;	}
		}

		@Entity
		class EntitySecondary {
			@Relation private EntityTertiary relation = new EntityTertiary();
			public EntityTertiary getRelation() { return relation;	}
		}

		@Entity
		class EntityPrimary {
			@Relation private EntitySecondary relation = new EntitySecondary();
			public EntitySecondary getRelation() { return relation;	}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("relation", "relation.relation"), null));

		assertThat("Size of result", result.size(), is(1));

		Map<String, Object> firstRelationResult = (Map<String, Object>) result.get("relation");

		assertThat("Size of firstRelation", firstRelationResult.size(), is(1));

		Map<String, Object> secondRelationResult = (Map<String, Object>) firstRelationResult.get("relation");

		assertThat("Size of secondRelationResult", secondRelationResult.size(), is(2));
		assertThat(secondRelationResult.get("first"), is("First"));
		assertThat(secondRelationResult.get("second"), is("Second"));
	}

	@Test
	public void attributesOnDoubleRelation() throws Exception {
		@Entity
		class EntityTertiary {
			@Attribute private String first = "First";
			public String getFirst() { return first;	}

			@Attribute private String second = "Second";
			public String getSecond() { return second;	}
		}

		@Entity
		class EntitySecondary {
			@Relation private EntityTertiary relation = new EntityTertiary();
			public EntityTertiary getRelation() { return relation;	}
		}

		@Entity
		class EntityPrimary {
			@Relation private EntitySecondary relation = new EntitySecondary();
			public EntitySecondary getRelation() { return relation;	}
		}

		EntityPrimary data = new EntityPrimary();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("relation.relation.first"), Arrays.asList("relation", "relation.relation"), null));

		assertThat("Size of result", result.size(), is(1));

		Map<String, Object> firstRelationResult = (Map<String, Object>) result.get("relation");

		assertThat("Size of firstRelation", firstRelationResult.size(), is(1));

		Map<String, Object> secondRelationResult = (Map<String, Object>) firstRelationResult.get("relation");

		assertThat("Size of secondRelationResult", secondRelationResult.size(), is(1));
		assertThat(secondRelationResult.get("first"), is("First"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void getterNotFound() throws Exception {
		@Entity
		class EntityWithNoGetterAndGetterWithWrongName {
			@Attribute
			private String noGetterAttribute = "Value for noGetterAttribute";

			@Attribute
			private String attributeWithWrongName = "Value for attributeWithWrongName";
			public String getWrongName() { return attributeWithWrongName; }
		}

		EntityWithNoGetterAndGetterWithWrongName data = new EntityWithNoGetterAndGetterWithWrongName();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));
	}

	@Test(expected = IllegalArgumentException.class)
	public void handleExceptions() throws Exception {
		@Entity
		class EntityWhichThrowsException {
			@Attribute
			private String value = "Value";
			public String getValue() { throw new RuntimeException("Exception thrown from getter"); }
		}

		EntityWhichThrowsException data = new EntityWhichThrowsException();

		translator.createJson(data, getParameters(null, null, null));
	}

	@Test
	public void superclassAsEntity() throws Exception {
		@Entity
		class Superclass {}

		class Subclass extends Superclass {
			@Attribute String str = "String";
			public String getStr() { return str; }
		}

		Subclass data = new Subclass();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));
		assertThat(result.get("str"), is(data.getStr()));
	}

	@Test
	public void superclassData() throws Exception {
		@Entity
		class Superclass {
			@Attribute String superStr = "SuperStr";
			public String getSuperStr() { return superStr;	}
		}

		class Subclass extends Superclass {
			@Attribute String str = "String";
			public String getStr() { return str; }
		}

		Subclass data = new Subclass();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(2));
		assertThat(result.get("superStr"), is(data.getSuperStr()));
		assertThat(result.get("str"), is(data.getStr()));
	}

	@Test
	public void superclassData_named() throws Exception {
		@Entity
		class Superclass {
			@Attribute String superStr = "SuperStr";
			public String getSuperStr() { return superStr;	}
		}

		class Subclass extends Superclass {
			@Attribute String str = "String";
			public String getStr() { return str; }
		}

		Subclass data = new Subclass();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("superStr"), null, null));

		assertThat("Size of result", result.size(), is(1));
		assertThat(result.get("superStr"), is(data.getSuperStr()));
	}

	@Test
	public void attributeOnMethod() throws Exception {
		@Entity
		class EntityWithAttributesOnMethods {
			@Attribute public String getGetMethod() { return "getMethod"; }
			@Attribute public String isIsMethod() { return "isMethod"; }
			@Attribute public String hasHasMethod() { return "hasMethod"; }
		}

		EntityWithAttributesOnMethods data = new EntityWithAttributesOnMethods();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(3));
		assertThat(result.get("getMethod"), is(data.getGetMethod()));
		assertThat(result.get("isMethod"), is(data.isIsMethod()));
		assertThat(result.get("hasMethod"), is(data.hasHasMethod()));
	}

	@Test
	public void mapOfObjects() throws Exception {
		@Entity
		class EntityClass {
			@Attribute String att;
			public String getAtt() { return att; }
			public EntityClass(String att) { this.att = att; }
		}

		Map<String, EntityClass> data = new LinkedHashMap<String, EntityClass>();
		data.put("one", new EntityClass("1"));
		data.put("two", new EntityClass("2"));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(2));

		Map<String, Object> first = (Map<String, Object>) result.get("one");
		assertThat(first.size(), is(1));
		assertThat(first.get("att"), is("1"));

		Map<String, Object> second = (Map<String, Object>) result.get("two");
		assertThat(second.size(), is(1));
		assertThat(second.get("att"), is("2"));
	}

	@Test
	public void mapInProperty() throws Exception {
		@Entity
		class EntityClass {
			@Attribute String att;
			public String getAtt() { return att; }
			public EntityClass(String att) { this.att = att; }
		}
		@Entity
		class EntityWithMapProperty {
			@Attribute Map<String, EntityClass> map;
			public Map<String, EntityClass> getMap() { return map; }
			public EntityWithMapProperty() {
				map = new HashMap<String, EntityClass>();
				map.put("one", new EntityClass("1"));
				map.put("two", new EntityClass("2"));
			}

		}

		EntityWithMapProperty data = new EntityWithMapProperty();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(1));

		Map<String, Object> map = (Map<String, Object>) result.get("map");
		assertThat(map.size(), is(2));

		Map<String, Object> first = (Map<String, Object>) map.get("one");
		assertThat(first.size(), is(1));
		assertThat(first.get("att"), is("1"));

		Map<String, Object> second = (Map<String, Object>) map.get("two");
		assertThat(second.size(), is(1));
		assertThat(second.get("att"), is("2"));
	}

	@Test
	public void mapWithAttributeNames() throws Exception {
		@Entity
		class EntityClass {
			@Attribute String att1;
			public String getAtt1() { return att1; }
			@Attribute String att2;
			public String getAtt2() { return att2; }
			public EntityClass(String att1, String att2) { this.att1 = att1; this.att2 = att2; }
		}

		Map<String, EntityClass> data = new LinkedHashMap<String, EntityClass>();
		data.put("one", new EntityClass("1", "2"));
		data.put("three", new EntityClass("3", "4"));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("att1"), null, null));

		assertThat("Size of result", result.size(), is(2));

		Map<String, Object> first = (Map<String, Object>) result.get("one");
		assertThat(first.size(), is(1));
		assertThat(first.get("att1"), is("1"));

		Map<String, Object> second = (Map<String, Object>) result.get("three");
		assertThat(second.size(), is(1));
		assertThat(second.get("att1"), is("3"));
	}

	@Test
	public void mapOfCollectionOrMap() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		Map<Object, Object> data = new LinkedHashMap<Object, Object>();

		EntitySimple entity1 = new EntitySimple(1);
		EntitySimple entity2 = new EntitySimple(2);
		EntitySimple entity3 = new EntitySimple(3);

		data.put("one", Arrays.asList(entity1, entity2));

		Map<Object, EntitySimple> internalData = new HashMap<Object, EntitySimple>();
		internalData.put("internal", entity3);

		data.put("two", internalData);

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(2));

		Collection<Object> first = (Collection<Object>) result.get("one");
		assertThat(first.size(), is(2));
		Iterator<Object> iter = first.iterator();

		Map<String, Object> firstInFirst = (Map<String, Object>) iter.next();
		assertThat(firstInFirst.size(), is(1));
		assertThat(firstInFirst.get("att"), is(1));

		Map<String, Object> secondInFirst = (Map<String, Object>) iter.next();
		assertThat(secondInFirst.size(), is(1));
		assertThat(secondInFirst.get("att"), is(2));

		Map<Object, Object> second = (Map<Object, Object>) result.get("two");
		assertThat(second.size(), is(1));
		Map<String, Object> mapOfSecondObject = (Map<String, Object>) second.get("internal");
		assertThat(mapOfSecondObject.size(), is(1));
		assertThat(mapOfSecondObject.get("att"), is(3));
	}

	@Test
	public void collectionOfCollectionOrMap() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		Collection<Object> data = new ArrayList<Object>();

		EntitySimple entity1 = new EntitySimple(1);
		EntitySimple entity2 = new EntitySimple(2);
		EntitySimple entity3 = new EntitySimple(3);

		data.add(Arrays.asList(entity1, entity2));

		Map<Object, EntitySimple> internalData = new HashMap<Object, EntitySimple>();
		internalData.put("internal", entity3);

		data.add(internalData);

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, null));

		assertThat("Size of result", result.size(), is(2));

		Iterator<Object> resultIter = result.iterator();

		Collection<Object> first = (Collection<Object>) resultIter.next();
		assertThat(first.size(), is(2));
		Iterator<Object> iter = first.iterator();

		Map<String, Object> firstInFirst = (Map<String, Object>) iter.next();
		assertThat(firstInFirst.size(), is(1));
		assertThat(firstInFirst.get("att"), is(1));

		Map<String, Object> secondInFirst = (Map<String, Object>) iter.next();
		assertThat(secondInFirst.size(), is(1));
		assertThat(secondInFirst.get("att"), is(2));

		Map<Object, Object> second = (Map<Object, Object>) resultIter.next();
		assertThat(second.size(), is(1));
		Map<String, Object> mapOfSecondObject = (Map<String, Object>) second.get("internal");
		assertThat(mapOfSecondObject.size(), is(1));
		assertThat(mapOfSecondObject.get("att"), is(3));
	}

	@Test
	public void mapOfCollection_withAttributes() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att1;
			public int getAtt1() { return att1; }
			@Attribute int att2;
			public int getAtt2() { return att2; }
			public EntitySimple(int att1, int att2) { this.att1 = att1; this.att2 = att2; }
		}

		Map<Object, Object> data = new LinkedHashMap<Object, Object>();

		EntitySimple entity1 = new EntitySimple(1, 11);
		EntitySimple entity2 = new EntitySimple(2, 22);

		data.put("one", Arrays.asList(entity1, entity2));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(Arrays.asList("att1"), null, null));

		assertThat("Size of result", result.size(), is(1));

		Collection<Object> first = (Collection<Object>) result.get("one");
		assertThat(first.size(), is(2));
		Iterator<Object> iter = first.iterator();

		Map<String, Object> firstInFirst = (Map<String, Object>) iter.next();
		assertThat(firstInFirst.size(), is(1));
		assertThat(firstInFirst.get("att1"), is(1));

		Map<String, Object> secondInFirst = (Map<String, Object>) iter.next();
		assertThat(secondInFirst.size(), is(1));
		assertThat(secondInFirst.get("att1"), is(2));
	}

	@Test
	public void sortInCollection_singleAttribute() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute Integer att;
			public Integer getAtt() { return att; }
			public EntitySimple(Integer att) { this.att = att; }
		}

		Collection<EntitySimple> data = Arrays.asList(new EntitySimple(5), new EntitySimple(2), new EntitySimple(null), new EntitySimple(8));

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("att"))));
		assertThat(result.size(), is(4));

		Iterator<Object> iter = result.iterator();

		Map<String, Object> first = (Map<String, Object>) iter.next();
		assertThat(first.get("att"), is(2));

		Map<String, Object> second = (Map<String, Object>) iter.next();
		assertThat(second.get("att"), is(5));

		Map<String, Object> third = (Map<String, Object>) iter.next();
		assertThat(third.get("att"), is(8));

		Map<String, Object> fourth = (Map<String, Object>) iter.next();
		assertNull(fourth.get("att"));
	}

	@Test
	public void sortInCollection_singleAttribute_descending() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute Integer att;
			public Integer getAtt() { return att; }
			public EntitySimple(Integer att) { this.att = att; }
		}

		Collection<EntitySimple> data = Arrays.asList(new EntitySimple(5), new EntitySimple(null), new EntitySimple(2), new EntitySimple(8));

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("-att"))));
		assertThat(result.size(), is(4));

		Iterator<Object> iter = result.iterator();

		Map<String, Object> first = (Map<String, Object>) iter.next();
		assertNull(first.get("att"));

		Map<String, Object> second = (Map<String, Object>) iter.next();
		assertThat(second.get("att"), is(8));

		Map<String, Object> third = (Map<String, Object>) iter.next();
		assertThat(third.get("att"), is(5));

		Map<String, Object> fourth = (Map<String, Object>) iter.next();
		assertThat(fourth.get("att"), is(2));
	}

	@Test
	public void sortInCollection_compoundAttribute() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att1;
			public int getAtt1() { return att1; }
			public EntitySimple(int att1) { this.att1 = att1;}
		}

		@Entity
		class EntityFront {
			@Attribute EntitySimple entitySimple;
			public EntitySimple getEntitySimple() { return entitySimple; }
			public EntityFront(int i) { this.entitySimple = new EntitySimple(i); };
		}

		Collection<EntityFront> data = Arrays.asList(new EntityFront(1), new EntityFront(2), new EntityFront(5), new EntityFront(3));

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("entitySimple.att1"))));

		assertThat(result.size(), is(4));

		Iterator<Object> iter = result.iterator();

		Map<String, Object> firstFront = (Map<String, Object>) iter.next();
		Map<String, Object> firstSimple = (Map<String, Object>) firstFront.get("entitySimple");
		assertThat(firstSimple.get("att1"), is(1));

		Map<String, Object> secondFront = (Map<String, Object>) iter.next();
		Map<String, Object> secondSimple = (Map<String, Object>) secondFront.get("entitySimple");
		assertThat(secondSimple.get("att1"), is(2));

		Map<String, Object> thirdFront = (Map<String, Object>) iter.next();
		Map<String, Object> thirdSimple = (Map<String, Object>) thirdFront.get("entitySimple");
		assertThat(thirdSimple.get("att1"), is(3));

		Map<String, Object> fourthFront = (Map<String, Object>) iter.next();
		Map<String, Object> fourthSimple = (Map<String, Object>) fourthFront.get("entitySimple");
		assertThat(fourthSimple.get("att1"), is(5));
	}

	@Test
	public void sortInCollection_multipleAttributes() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att1;
			public int getAtt1() { return att1; }
			@Attribute int att2;
			public int getAtt2() { return att2; }
			public EntitySimple(int att1, int att2) { this.att1 = att1; this.att2 = att2;}
		}

		Collection<EntitySimple> data = Arrays.asList(new EntitySimple(1, 6), new EntitySimple(2, 4), new EntitySimple(2, 3), new EntitySimple(5, 4));

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("att1"), srp("att2"))));
		assertThat(result.size(), is(4));

		Iterator<Object> iter = result.iterator();

		Map<String, Object> first = (Map<String, Object>) iter.next();
		assertThat(first.get("att1"), is(1));

		Map<String, Object> second = (Map<String, Object>) iter.next();
		assertThat(second.get("att1"), is(2));
		assertThat(second.get("att2"), is(3));

		Map<String, Object> third = (Map<String, Object>) iter.next();
		assertThat(third.get("att1"), is(2));
		assertThat(third.get("att2"), is(4));

		Map<String, Object> fourth = (Map<String, Object>) iter.next();
		assertThat(fourth.get("att1"), is(5));
	}

	@Test(expected = IllegalArgumentException.class)
	public void sortInCollection_usingMAPKEY() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		Collection<EntitySimple> data = Arrays.asList(new EntitySimple(5), new EntitySimple(2));

		Collection<Object> result = (Collection<Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("-MAPKEY"))));
	}

	@Test
	public void sortInMap_singleAttribute() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		Map<String, EntitySimple> data = new LinkedHashMap<String, EntitySimple>();
		data.put("1", new EntitySimple(4));
		data.put("2", new EntitySimple(2));
		data.put("3", new EntitySimple(3));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("att"))));
		assertThat(result.size(), is(3));

		Iterator<String> keyIter = result.keySet().iterator();

		Map<String, Object> first = (Map<String, Object>) result.get(keyIter.next());
		assertThat(first.get("att"), is(2));

		Map<String, Object> second = (Map<String, Object>) result.get(keyIter.next());
		assertThat(second.get("att"), is(3));

		Map<String, Object> third = (Map<String, Object>) result.get(keyIter.next());
		assertThat(third.get("att"), is(4));
	}

	@Test
	public void sortInMap_singleAttribute_descending() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		Map<String, EntitySimple> data = new LinkedHashMap<String, EntitySimple>();
		data.put("1", new EntitySimple(4));
		data.put("2", new EntitySimple(2));
		data.put("3", new EntitySimple(3));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("-att"))));
		assertThat(result.size(), is(3));

		Iterator<String> keyIter = result.keySet().iterator();

		Map<String, Object> first = (Map<String, Object>) result.get(keyIter.next());
		assertThat(first.get("att"), is(4));

		Map<String, Object> second = (Map<String, Object>) result.get(keyIter.next());
		assertThat(second.get("att"), is(3));

		Map<String, Object> third = (Map<String, Object>) result.get(keyIter.next());
		assertThat(third.get("att"), is(2));
	}

	@Test
	public void sortInMap_compoundAttribute() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att1;
			public int getAtt1() { return att1; }
			public EntitySimple(int att1) { this.att1 = att1;}
		}

		@Entity
		class EntityFront {
			@Attribute EntitySimple entitySimple;
			public EntitySimple getEntitySimple() { return entitySimple; }
			public EntityFront(int i) { entitySimple = new EntitySimple(i); }
		}

		Map<String, EntityFront> data = new LinkedHashMap<String, EntityFront>();
		data.put("1", new EntityFront(4));
		data.put("2", new EntityFront(2));
		data.put("3", new EntityFront(3));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("entitySimple.att1"))));
		assertThat(result.size(), is(3));

		Iterator<String> keyIter = result.keySet().iterator();

		String key1 = keyIter.next();
		Map<String, Object> front1 = (Map<String, Object>) result.get(key1);
		Map<String, Object> simple1 = (Map<String, Object>) front1.get("entitySimple");
		assertThat(simple1.get("att1"), is(2));

		String key2 = keyIter.next();
		Map<String, Object> front2 = (Map<String, Object>) result.get(key2);
		Map<String, Object> simple2 = (Map<String, Object>) front2.get("entitySimple");
		assertThat(simple2.get("att1"), is(3));

		String key3 = keyIter.next();
		Map<String, Object> front3 = (Map<String, Object>) result.get(key3);
		Map<String, Object> simple3 = (Map<String, Object>) front3.get("entitySimple");
		assertThat(simple3.get("att1"), is(4));
	}

	@Test
	public void sortInMap_multipleAttributes() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att1;
			public int getAtt1() { return att1; }
			@Attribute int att2;
			public int getAtt2() { return att2; }
			public EntitySimple(int att1, int att2) { this.att1 = att1; this.att2 = att2;}
		}

		Map<String, EntitySimple> data = new LinkedHashMap<String, EntitySimple>();
		data.put("1", new EntitySimple(1,6));
		data.put("2", new EntitySimple(2,4));
		data.put("3", new EntitySimple(2,3));
		data.put("4", new EntitySimple(5,4));

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(srp("att1"), srp("att2"))));
		assertThat(result.size(), is(4));

		Iterator<String> iter = result.keySet().iterator();

		Map<String, Object> first = (Map<String, Object>) result.get(iter.next());
		assertThat(first.get("att1"), is(1));

		Map<String, Object> second = (Map<String, Object>) result.get(iter.next());
		assertThat(second.get("att1"), is(2));
		assertThat(second.get("att2"), is(3));

		Map<String, Object> third = (Map<String, Object>) result.get(iter.next());
		assertThat(third.get("att1"), is(2));
		assertThat(third.get("att2"), is(4));

		Map<String, Object> fourth = (Map<String, Object>) result.get(iter.next());
		assertThat(fourth.get("att1"), is(5));
	}

	@Test
	public void sortInnerCollection() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		@Entity
		class EntityWithArrayOfSimple {
			@Attribute List<EntitySimple> array = new ArrayList<EntitySimple>();
			public List<EntitySimple> getArray() { return array; }
			public EntityWithArrayOfSimple(int... atts) {
				for (int att : atts) {
					array.add(new EntitySimple(att));
				}
			}
		}

		EntityWithArrayOfSimple data = new EntityWithArrayOfSimple(5,2,4,6);

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(new SortParameter("array", "att"))));
		assertThat(result.size(), is(1));

		Collection<Map<String, Object>> collectionResult = (Collection<Map<String, Object>>) result.get("array");
		assertThat(collectionResult.size(), is(4));

		Iterator<Map<String, Object>> iter = collectionResult.iterator();

		assertThat(iter.next().get("att"), is(2));
		assertThat(iter.next().get("att"), is(4));
		assertThat(iter.next().get("att"), is(5));
		assertThat(iter.next().get("att"), is(6));
	}

	@Test
	public void sortInnerCollection_doNotSortOuterCollection() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		List<EntitySimple> data = Arrays.asList(new EntitySimple(5), new EntitySimple(2), new EntitySimple(6));

		Collection<Map<String, Object>> result = (Collection<Map<String, Object>>) translator.createJson(data, getParameters(null, null, Arrays.asList(new SortParameter("array", "att"))));
		assertThat(result.size(), is(3));

		Iterator<Map<String, Object>> iter = result.iterator();
		assertThat(iter.next().get("att"), is(5));
		assertThat(iter.next().get("att"), is(2));
		assertThat(iter.next().get("att"), is(6));
	}

	@Test
	public void sortInnerCollection_doNotAffectOtherCollections() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int att;
			public int getAtt() { return att; }
			public EntitySimple(int att) { this.att = att; }
		}

		@Entity
		class EntityWithArrayOfSimple {
			@Attribute List<EntitySimple> array = new ArrayList<EntitySimple>();
			public List<EntitySimple> getArray() { return array; }
			@Attribute List<Integer> ints = Arrays.asList(5,3,2,6);
			public List<Integer> getInts() { return ints; };
			public EntityWithArrayOfSimple(int... atts) {
				for (int att : atts) {
					array.add(new EntitySimple(att));
				}
			}
		}

		EntityWithArrayOfSimple data = new EntityWithArrayOfSimple(5,2,4,6);

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(new SortParameter("array", "att"))));
		assertThat(result.size(), is(2));

		Collection<Map<String, Object>> collectionResult = (Collection<Map<String, Object>>) result.get("array");
		assertThat(collectionResult.size(), is(4));

		Iterator<Map<String, Object>> iter = collectionResult.iterator();

		assertThat(iter.next().get("att"), is(2));
		assertThat(iter.next().get("att"), is(4));
		assertThat(iter.next().get("att"), is(5));
		assertThat(iter.next().get("att"), is(6));

		Collection<Integer> intArray = (Collection<Integer>) result.get("ints");
		Iterator<Integer> intIter = intArray.iterator();
		assertThat(intIter.next(), is(5));
		assertThat(intIter.next(), is(3));
		assertThat(intIter.next(), is(2));
		assertThat(intIter.next(), is(6));
	}

	@Test
	public void sortDeeperCollection() throws Exception {
		@Entity
		class EntitySimple {
			@Attribute int data;
			public int getData() { return data; }
			public EntitySimple(int data) { this.data = data; }
		}

		@Entity
		class EntityWithArrayOfSimple {
			@Attribute List<EntitySimple> array = new ArrayList<EntitySimple>();
			public List<EntitySimple> getArray() { return array; }
			public EntityWithArrayOfSimple(int... atts) {
				for (int att : atts) {
					array.add(new EntitySimple(att));
				}
			}
		}

		@Entity
		class EntityWithAttributeOfEntityWithArrayOfSimple {
			@Attribute EntityWithArrayOfSimple att;
			public EntityWithArrayOfSimple getAtt() { return att; }
			public EntityWithAttributeOfEntityWithArrayOfSimple(int... values) {
				att = new EntityWithArrayOfSimple(values);
			}
		}

		EntityWithAttributeOfEntityWithArrayOfSimple data = new EntityWithAttributeOfEntityWithArrayOfSimple(5, 2, 4, 6);

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, null, Arrays.asList(new SortParameter("att.array", "data"))));
		assertThat(result.size(), is(1));

		Map<String, Object> inner = (Map<String, Object>) result.get("att");
		assertThat(inner.size(), is(1));

		Collection<Map<String, Object>> collection = (Collection<Map<String, Object>>) inner.get("array");
		assertThat(collection.size(), is(4));

		Iterator<Map<String, Object>> iter = collection.iterator();

		assertThat(iter.next().get("data"), is(2));
		assertThat(iter.next().get("data"), is(4));
		assertThat(iter.next().get("data"), is(5));
		assertThat(iter.next().get("data"), is(6));
	}

	@Test
	public void sortVeryDeep() throws Exception {
		@Entity
		class Third {
			@Attribute int att;
			public int getAtt() { return att; }
			public Third(int att) { this.att = att; }
		}

		@Entity
		class Second {
			@Attribute List<Third> third = new ArrayList();
			public List<Third> getThird() { return third; }
		}

		@Entity
		class First {
			@Attribute Second second;
			public Second getSecond() { return second; }
		}

		@Entity
		class Data {
			@Attribute Set<First> first = new HashSet<>();
			public Set<First> getFirst() { return first; }
		}

		Second second = new Second();
		second.third = Arrays.asList(new Third(3), new Third(1), new Third(5), new Third(2));

		First first = new First();
		first.second = second;

		Data data = new Data();
		data.getFirst().add(first);

		SortParameter sortParameter = new SortParameter("first.second.third", "att");

		ParameterData parameters = new ParameterData();
		parameters.setSortParameters(Arrays.asList(sortParameter));


		Map<String, Object> dataResult = (Map<String, Object>) translator.createJson(data, parameters);


		Collection<Map<String, Object>> firstResult = (Collection<Map<String, Object>>) dataResult.get("first");

		Map<String, Object> secondResult = (Map<String, Object>) firstResult.iterator().next().get("second");

		Collection<Map<String, Object>> thirdResult = (Collection<Map<String, Object>>) secondResult.get("third");

		assertThat(thirdResult.size(), is(4));

		Iterator<Map<String, Object>> iter = thirdResult.iterator();

		int att1 = (int) iter.next().get("att");
		int att2 = (int) iter.next().get("att");
		int att3 = (int) iter.next().get("att");
		int att4 = (int) iter.next().get("att");

		assertThat(att1, is(1));
		assertThat(att2, is(2));
		assertThat(att3, is(3));
		assertThat(att4, is(5));
	}


	@Test
	public void proxyWithDerivedMethod() throws Exception {
		@Entity
		class Real {
			@Attribute String getDerived() {
				return "derived value";
			}
		}

		class Proxy extends Real {
			private final Real realObject = new Real();

			@Override
			String getDerived() {
				return this.realObject.getDerived();
			}
		}

		@Entity
		class Start {
			@Relation private Real rel = new Proxy();
			public Real getRel() { return this.rel; }
		}


		Start data = new Start();

		Map<String, Object> result = (Map<String, Object>) translator.createJson(data, getParameters(null, Arrays.asList("rel"), null));

		assertThat(result.size(), is(1));

		Map<String, Object> proxyResult = (Map<String, Object>) result.get("rel");
		assertThat(proxyResult.size(), is(1));
		assertThat(proxyResult.get("derived"), is("derived value"));
	}



	private ParameterData getParameters(Collection<String> attributeNames, Collection<String> relationNames, List<SortParameter> sortParameters) {
		ParameterData ret = new ParameterData();
		ret.setAttributeNames(attributeNames);
		ret.setRelationNames(relationNames);
		ret.setSortParameters(sortParameters);
		return ret;
	}

	private SortParameter srp(String name) {
		return new SortParameter(name);
	}



}
