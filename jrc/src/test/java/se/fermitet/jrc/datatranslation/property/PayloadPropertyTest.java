package se.fermitet.jrc.datatranslation.property;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Payload;


public class PayloadPropertyTest {

	private class TestData {
		@Payload
		private Object payload;
		
		@Attribute 
		private Object attribute;
		
		@SuppressWarnings("unused")
		private Object nonAnnotated;
	}
	
	private class TestDataWithoutPayload {
		@SuppressWarnings("unused")
		private Object object;
	}
	
	private class TestDataWithDoublePayload {
		@Payload
		private Object p1;
		
		@Payload 
		private Object p2;
	}
	
	@Test
	public void canCreateSingle_whenPayloadAnnotated() throws Exception {
		PayloadProperty prop = new PayloadProperty("payload", TestData.class);
		assertNotNull(prop);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void cannotCreateSingle_whenOhterAnnotation() throws Exception {
		new PayloadProperty("attribute", TestData.class);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void cannotCreateSingle_whenNotAnnotated() throws Exception {
		new PayloadProperty("nonAnnotated", TestData.class);
	}
	
	@Test
	public void getPayloadProperty_withPayload() throws Exception {
		PayloadProperty prop = PayloadProperty.getPayloadProperty(TestData.class);
		assertThat(prop.getName(), is("payload"));
	}
	
	@Test
	public void getPayloadProperty_withoutPayload() throws Exception {
		assertNull(PayloadProperty.getPayloadProperty(TestDataWithoutPayload.class));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getPayload_doublePayload() throws Exception {
		PayloadProperty.getPayloadProperty(TestDataWithDoublePayload.class);
	}
	
	@Test
	public void hasDefinedPayload() throws Exception {
		assertThat(PayloadProperty.hasDefinedPayload(TestData.class), is(true));
		assertThat(PayloadProperty.hasDefinedPayload(TestDataWithoutPayload.class), is(false));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void hasDefinedPayload_onDoublePayload() throws Exception {
		PayloadProperty.hasDefinedPayload(TestDataWithDoublePayload.class);
	}

}
