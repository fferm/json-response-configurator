package se.fermitet.jrc.example.webshop.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class MoneyTest {

	@Test
	public void testToString() throws Exception {
		assertThat(new Money("100,00").toString(), is("100,00"));
	}
	
	@Test
	public void valueObject() throws Exception {
		Money orig = new Money("100,00");
		Money same = new Money("100,00");
		Money other = new Money("200,00");
		
		assertTrue(orig.equals(orig));
		assertTrue(orig.equals(same));
		assertFalse(orig.equals(other));
		assertFalse(orig.equals("OTHER CLASS"));
		assertFalse(orig.equals(null));
		
		assertTrue(orig.hashCode() == same.hashCode());
		
	}
	@Test
	public void multiply() throws Exception {
		Money orig = new Money("100,00");
		
		assertThat(orig.multiply(2), is(new Money("200,00")));
	}
	
	@Test
	public void add() throws Exception {
		assertThat(new Money("100,00").add(new Money("10,00")), is(new Money("110,00")));
	}
	
	@Test
	public void comparable() throws Exception {
		Money m = new Money("100,00");
		Money same = new Money("100,00");
		Money larger = new Money("200,00");
		Money smaller = new Money("50,00");
		
		assertThat((m instanceof Comparable), is(true));
		
		assertThat(m.compareTo(same), is(0));
		assertThat(m.compareTo(larger), is(lessThan(0)));
		assertThat(m.compareTo(smaller), is(greaterThan(0)));
	}
}
