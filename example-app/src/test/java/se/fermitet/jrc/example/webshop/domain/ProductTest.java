package se.fermitet.jrc.example.webshop.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {
	
	@Test
	public void product_hasValidConstructor() throws Exception {
		assertThat(Product.class, hasValidBeanConstructor());
	}
	
	@Test
	public void product_hasValidGettersAndSetters() throws Exception {
		assertThat(Product.class, hasValidGettersAndSetters());
	}
	
	@Test
	public void product_hasValidToString() throws Exception {
		assertThat(Product.class, hasValidBeanToString());
	}
	
}
