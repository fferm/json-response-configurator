package se.fermitet.jrc.example.games.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PlayerTest {
	@Test
	public void _hasValidConstructor() throws Exception {
		assertThat(Player.class, hasValidBeanConstructor());
	}
	
	@Test
	public void _hasValidGettersAndSetters() throws Exception {
		assertThat(Player.class, hasValidGettersAndSetters());
	}

}
