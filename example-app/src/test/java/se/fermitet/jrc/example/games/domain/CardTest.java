package se.fermitet.jrc.example.games.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class CardTest {
	@Test
	public void _hasValidConstructor() throws Exception {
		assertThat(Card.class, hasValidBeanConstructor());
	}
	
	@Test
	public void _hasValidGettersAndSetters() throws Exception {
		assertThat(Card.class, hasValidGettersAndSetters());
	}

}
