package se.fermitet.jrc.example.webshop.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import com.google.code.beanmatchers.BeanMatchers;
import com.google.code.beanmatchers.ValueGenerator;

public class OrderTest {
	@Before
	public void setUp() {
		BeanMatchers.registerValueGenerator(new ValueGenerator<LocalDate>() {
			@Override
			public LocalDate generate() {
				return LocalDate.now();
			}
		}, LocalDate.class);
	}
	@Test
	public void order_hasValidConstructor() throws Exception {
		assertThat(Order.class, hasValidBeanConstructor());
	}
	
	@Test
	public void order_hasValidGettersAndSetters() throws Exception {
		assertThat(Order.class, hasValidGettersAndSettersExcluding("orderLines", "totalSum"));
	}
	
	@Test
	public void order_hasValidToString() throws Exception {
		assertThat(Order.class, hasValidBeanToStringExcluding("orderLines", "totalSum"));
	}
	
	@Test
	public void totalSum() throws Exception {
		Order order = new Order();
		
		OrderLine ol1 = new OrderLine();
		ol1.setQuantity(1);
		Product p1 = new Product();
		p1.setUnitPrice(new Money("100,00"));
		ol1.setProduct(p1);
		order.addOrderLine(ol1);

		OrderLine ol2 = new OrderLine();
		ol2.setQuantity(2);
		Product p2 = new Product();
		p2.setUnitPrice(new Money("25,00"));
		ol2.setProduct(p2);
		order.addOrderLine(ol2);
		
		assertThat(order.getTotalSum(), is(new Money("150,00")));
	}

}
