package se.fermitet.jrc.example.games.domain;

import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSettersExcluding;
import static org.junit.Assert.*;

public class SeasonTest {
	@Test
	public void _hasValidConstructor() throws Exception {
		assertThat(Season.class, hasValidBeanConstructor());
	}

	@Test
	public void _hasValidGettersAndSetters() throws Exception {
		assertThat(Season.class, hasValidGettersAndSettersExcluding("games"));
	}

}