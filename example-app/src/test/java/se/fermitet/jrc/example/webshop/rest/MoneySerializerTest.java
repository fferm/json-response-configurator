package se.fermitet.jrc.example.webshop.rest;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonGenerator;

import se.fermitet.jrc.example.webshop.domain.Money;

@RunWith(MockitoJUnitRunner.class)
public class MoneySerializerTest {
	@Mock
	private JsonGenerator jgen;
	
	@InjectMocks
	private MoneySerializer obj;
	
	@Test
	public void printsMoneyAmountAsString() throws Exception {
		Money m = new Money("100,00");
		obj.serialize(m, jgen, null);

		verify(jgen).writeString("100,00");
	}
}
