package se.fermitet.jrc.example.rest;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;


public class DataPackageTest {
	@Test
	public void dataPackage_hasValidConstructor() throws Exception {
		assertThat(DataPackage.class, hasValidBeanConstructor());
	}
	
	@Test
	public void dataPackage_hasValidGettersAndSetters() throws Exception {
		assertThat(DataPackage.class, hasValidGettersAndSetters());
	}
	
	@Test
	public void dataPackage_hasValidToString() throws Exception {
		assertThat(DataPackage.class, hasValidBeanToString());
	}
}
