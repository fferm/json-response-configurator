package se.fermitet.jrc.example.games.domain;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import com.google.code.beanmatchers.BeanMatchers;
import com.google.code.beanmatchers.ValueGenerator;

public class GameTest {
	@Before
	public void setUp() {
		BeanMatchers.registerValueGenerator(new ValueGenerator<LocalDateTime>() {
			@Override
			public LocalDateTime generate() {
				return LocalDateTime.now();
			}
		}, LocalDateTime.class);
	}

	@Test
	public void _hasValidConstructor() throws Exception {
		assertThat(Game.class, hasValidBeanConstructor());
	}
	
	@Test
	public void _hasValidGettersAndSetters() throws Exception {
		assertThat(Game.class, hasValidGettersAndSettersExcluding("homeGoals", "awayGoals", "goals", "cards", "changes"));
	}
	
	@Test
	public void numGoalsWhenEmpty() throws Exception {
		Game game = new Game();
		assertThat(game.getHomeGoals(), is(0));
		assertThat(game.getAwayGoals(), is(0));
	}
	
	@Test
	public void numGoalsWhenAddingGoals() throws Exception {
		Team home = new Team();
		Team away = new Team();
		
		Game game = new Game(home, away);
		
		Goal g1 = new Goal(home, 10);
		Goal g2 = new Goal(away, 11);
		Goal g3 = new Goal(home, 15);
		
		game.addGoal(g1);
		game.addGoal(g2);
		game.addGoal(g3);
		
		assertThat(game.getHomeGoals(), is(2));
		assertThat(game.getAwayGoals(), is(1));
	}
	
}
