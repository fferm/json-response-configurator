package se.fermitet.jrc.example.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Season {
	@Attribute
	private String league;

	@Attribute
	private String year;

	@Relation
	private Collection<Game> games;

	public Season() {
		super();
		this.games = new ArrayList<>();
	}

	public Season(String league, String year) {
		this();
		setLeague(league);
		setYear(year);
	}

	public String getLeague() {
		return this.league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Collection<Game> getGames() {
		return this.games;
	}

	public void addGame(Game game) {
		getGames().add(game);
	}

}
