package se.fermitet.jrc.example.rest;

import se.fermitet.jrc.annotation.Payload;


public class DataPackage {
	@Payload
	private Object payload;
	private int status;
	private String message;
	
	public Object getPayload() {
		return payload;
	}
	public void setPayload(Object payload) {
		this.payload = payload;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("DataPackage: {");
		buf.append("payload: ");
		buf.append(getPayload());
		buf.append(", status: ");
		buf.append(getStatus());
		buf.append(", message: ");
		buf.append(getMessage());
		buf.append("}");
		
		return buf.toString();
	}
}
