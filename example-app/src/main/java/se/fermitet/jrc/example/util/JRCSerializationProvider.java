package se.fermitet.jrc.example.util;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Provider
public class JRCSerializationProvider implements ContextResolver<ObjectMapper> {

	private ObjectMapper defaultObjectMapper;
	
	@Override
	public ObjectMapper getContext(Class<?> type) {
		if (defaultObjectMapper == null)
			defaultObjectMapper = createDefaultMapper();
		
		return defaultObjectMapper;
	}

	private ObjectMapper createDefaultMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JRCSerializationModule());
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		return mapper;
	}

}
