package se.fermitet.jrc.example.webshop.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;

@Entity
public class Product {
	@Attribute
	private Integer productId;
	
	@Attribute
	private String name;
	
	@Attribute
	private String description;
	
	@Attribute
	private String imageUrl;
	
	@Attribute
	private Money unitPrice;
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String url) {
		this.imageUrl = url;
	}
	public Money getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Money unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("Product ");
		buf.append("{");
		buf.append("productId: ");
		buf.append(getProductId());
		buf.append(", name: ");
		buf.append(getName());
		buf.append(", description: ");
		buf.append(getDescription());
		buf.append(", imageUrl: ");
		buf.append(getImageUrl());
		buf.append(", unitPrice: ");
		buf.append(getUnitPrice());
		buf.append("}");
		
		return buf.toString();
	}
	
	
}
