package se.fermitet.jrc.example.webshop.data;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import se.fermitet.jrc.example.webshop.domain.Customer;
import se.fermitet.jrc.example.webshop.domain.Money;
import se.fermitet.jrc.example.webshop.domain.Order;
import se.fermitet.jrc.example.webshop.domain.OrderLine;
import se.fermitet.jrc.example.webshop.domain.Product;

@ApplicationScoped
public class ExampleData {
	private Map<Integer, Customer> customers;
	private Map<Integer, Product> products;
	
	public ExampleData() {
		super();
		
		initProducts();
		initCustomers();
	}
	
	public Collection<Customer> getAllCustomers() {
		return customers.values();
	}

	public Customer getCustomerById(Integer customerId) {
		return customers.get(customerId);
	}

	private void initProducts() {
		products = new HashMap<Integer, Product>();
		
		Product p1 = createGizmoGadget();
		products.put(p1.getProductId(), p1);
				
		Product p2 = createThingamajig();
		products.put(p2.getProductId(), p2);
	}

	private void initCustomers() {
		customers = new HashMap<Integer, Customer>();

		Customer cust1 = getAlbert();
		customers.put(cust1.getCustomerId(), cust1);
		
		Customer cust2 = getCharlie();
		customers.put(cust2.getCustomerId(), cust2);
	}

	private Product createGizmoGadget() {
		return createProduct(1, "Gizmo Gadget", "A really cool gadget", "http://my.store.com/gizmo.jpg", new Money("100,00"));
	}
	
	private Product createThingamajig() {
		return createProduct(2, "Thingamajig", "The thing everybody wants", "http://my.store.com/thingamajig.jpg", new Money("125,00"));
	}
	
	private Product createProduct(int productId, String name, String description, String url, Money unitPrice) {
		Product p = new Product();
		p.setProductId(productId);
		p.setName(name);
		p.setDescription(description);
		p.setImageUrl(url);
		p.setUnitPrice(unitPrice);
		
		return p;
	}
	
	private Customer getAlbert() {
		Customer customer = new Customer();

		customer.setCustomerId(12);
		customer.setName("Albert Broom");
		customer.setAddress("12 Big street, The city");
		
		Order order1 = createOrder(123453, LocalDate.of(2017, 3, 6), createOrderLine(1, products.get(1)));
		customer.addOrder(order1);
		
		Order order2 = createOrder(5244, LocalDate.of(2016, 12, 23), createOrderLine(2, products.get(2)));
		customer.addOrder(order2);
		
		return customer;
	}
	
	private Customer getCharlie() {
		Customer customer = new Customer();
		
		customer.setCustomerId(9);
		customer.setName("Charlie Duke");
		customer.setAddress("1 Rural road, Farmers fields");
		
		Order o1 = createOrder(2131, LocalDate.of(2017, 5, 3), createOrderLine(6, products.get(2)));
		customer.addOrder(o1);
		
		return customer;
	}

	private Order createOrder(Integer orderId, LocalDate date, OrderLine... orderLines) {
		Order order = new Order();
		order.setOrderId(orderId);
		order.setDate(date);
		for (OrderLine line : orderLines) {
			order.addOrderLine(line);
		}
		return order;
	}
	private OrderLine createOrderLine(int quantity, Product product) {
		OrderLine ol1 = new OrderLine();
		ol1.setQuantity(quantity);
		ol1.setProduct(product);
		return ol1;
	}
}
