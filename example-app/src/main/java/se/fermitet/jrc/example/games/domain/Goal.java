package se.fermitet.jrc.example.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class Goal {
	@Attribute
	private int minute;
	
	@Attribute
	private Team team;

	@Relation
	private Player player;
	
	@Relation
	private Player assist;
	
	public Goal() {
		super();
	}

	public Goal(Team team, int minute) {
		super();
		this.minute = minute;
		this.team = team;
	}
	
	public Goal(Team team, int minute, Player player, Player assist) {
		this(team, minute);
		this.player = player;
		this.assist = assist;
	}
	
	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getAssist() {
		return assist;
	}

	public void setAssist(Player assist) {
		this.assist = assist;
	}
	
	
}
