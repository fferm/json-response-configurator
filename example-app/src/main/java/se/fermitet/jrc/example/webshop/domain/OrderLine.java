package se.fermitet.jrc.example.webshop.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class OrderLine {
	
	@Attribute
	private int quantity;
	
	@Relation
	private Product product;
	
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("OrderLine ");
		buf.append("{");
		buf.append("quantity: ");
		buf.append(getQuantity());
		buf.append(", product: ");
		buf.append(getProduct());
		buf.append("}");
		
		return buf.toString();
	}
}
