package se.fermitet.jrc.example.games.data;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import se.fermitet.jrc.example.games.domain.Arena;
import se.fermitet.jrc.example.games.domain.Card;
import se.fermitet.jrc.example.games.domain.Card.Type;
import se.fermitet.jrc.example.games.domain.Change;
import se.fermitet.jrc.example.games.domain.Game;
import se.fermitet.jrc.example.games.domain.Goal;
import se.fermitet.jrc.example.games.domain.Player;
import se.fermitet.jrc.example.games.domain.Team;

public class GamesDataGenerator {


	private final Arena norrporten = new Arena("Norrporten arena", "Sundsvall", 8500);
	private final Arena borasArena = new Arena("Borås arena", "Borås", 15911);

	private final Team sundsvall = new Team("GIF Sundsvall", "Sundsvall");
	private final Player moros = new Player("Carlos Moros Garcia", 16);
	private final Player sundberg = new Player("Noah Sonko Sundberg", 19);
	private final Player naurin = new Player("Tommy Naurin", 17);
	private final Player rajalasko = new Player("Sebastian Rajalasko", 11);
	private final Player myrestam = new Player("David Myrestam", 4);
	private final Player pirttijoki = new Player("Juho Pirttijoki", 2);
	private final Player sigurdsson = new Player("Kristinn Freyr Sigurdssson", 6);
	private final Player morsay = new Player("Jonathan Morsay", 32);

	private final Team hammarby = new Team("Hammarby", "Stockholm");
	private final Player svendsen = new Player("Sander Svendsen", 21);
	private final Player paulsen = new Player("Björn Paulsen", 4);
	private final Player smarason = new Player("Arnor Smarason", 11);
	private final Player saevarsson = new Player("Birkir Már Sævarsson", 2);
	private final Player bakir = new Player("Kennedy Bakircioglu", 10);
	private final Player andersen = new Player("Jeppe Andersen", 8);
	private final Player tankovic = new Player("Muamer Tankovic", 22);
	private final Player bengtsson = new Player("Leo Bengtsson", 34);

	private final Team elfsborg = new Team("Elfsborg", "Borås");
	private final Player lundevall = new Player("Simon Lundevall", 10);
	private final Player jonsson = new Player("Jon Jönsson", 6);
	private final Player nilsson = new Player("Joakim Nilsson", 12);
	private final Player olsson = new Player("Simon Olsson", 18);
	private final Player dyer = new Player("Alex Dyer", 23);
	private final Player gojani = new Player("Robert Gojani", 16);
	private final Player prodell = new Player("Viktor Prodell", 22);
	private final Player ishizaki = new Player("Stefan Ishizaki", 24);
	private final Player dresevic = new Player("Ibrahim Dresevic", 14);

	private final Team djurgarden = new Team("Djurgården", "Stockholm");
	private final Player ring = new Player("Jonathan Ring", 11);
	private final Player badji = new Player("Aliou Badji", 20);
	private final Player ulvestad = new Player("Fredrik Ulvestad", 23);
	private final Player karlstrom = new Player("Jesper Karlström", 6);
	private final Player kozica = new Player("Dzenis Kozica", 7);
	private final Player kadewere = new Player("Tinotenda Kadewere", 24);
	private final Player djurgardenOlsson = new Player("Jonas Olsson", 13);
	private final Player augustinsson = new Player("Jonathan Augustinsson", 15);

	private Game sundsvallHammarby;
	private Game elfsborgDjurgarden;

	public GamesDataGenerator() {
		super();

		createSundsvallHammarby();
		createElfsborgDjurgarden();
	}

	private void createSundsvallHammarby() {
		sundsvallHammarby = new Game(sundsvall, hammarby, LocalDateTime.of(2017,10,29,15,0,0), 5157, norrporten);

		sundsvallHammarby.addGoal(new Goal(sundsvall, 17, moros, sundberg));
		sundsvallHammarby.addGoal(new Goal(hammarby, 42, svendsen, saevarsson));
		sundsvallHammarby.addGoal(new Goal(hammarby, 52, svendsen, bakir));
		sundsvallHammarby.addGoal(new Goal(hammarby, 68, paulsen, null));
		sundsvallHammarby.addGoal(new Goal(hammarby, 91, smarason, svendsen));

		sundsvallHammarby.addCard(new Card(hammarby, 44, Type.YELLOW, andersen));
		sundsvallHammarby.addCard(new Card(sundsvall, 75, Type.YELLOW, naurin));

		sundsvallHammarby.addChange(new Change(sundsvall, 65, rajalasko, myrestam));
		sundsvallHammarby.addChange(new Change(sundsvall, 73, pirttijoki, moros));
		sundsvallHammarby.addChange(new Change(sundsvall, 78, sigurdsson, morsay));
		sundsvallHammarby.addChange(new Change(hammarby, 82, bengtsson, tankovic));
	}

	private void createElfsborgDjurgarden() {
		elfsborgDjurgarden = new Game(elfsborg, djurgarden, LocalDateTime.of(2018, 4, 23, 19, 0, 0), 6540, borasArena);

		elfsborgDjurgarden.addGoal(new Goal(elfsborg, 9, lundevall, dyer));
		elfsborgDjurgarden.addGoal(new Goal(elfsborg, 90, jonsson, null));
		elfsborgDjurgarden.addGoal(new Goal(djurgarden, 21, ring, karlstrom));
		elfsborgDjurgarden.addGoal(new Goal(djurgarden, 45, badji, ring));

		elfsborgDjurgarden.addCard(new Card(elfsborg, 70, Type.YELLOW, gojani));
		elfsborgDjurgarden.addCard(new Card(elfsborg, 80, Type.YELLOW, dresevic));
		elfsborgDjurgarden.addCard(new Card(elfsborg, 80, Type.YELLOW, olsson));
		elfsborgDjurgarden.addCard(new Card(djurgarden, 86, Type.YELLOW, augustinsson));
		elfsborgDjurgarden.addCard(new Card(djurgarden, 89, Type.YELLOW, djurgardenOlsson));

		elfsborgDjurgarden.addChange(new Change(elfsborg, 16, jonsson, nilsson));
		elfsborgDjurgarden.addChange(new Change(elfsborg, 61, olsson, dyer));
		elfsborgDjurgarden.addChange(new Change(elfsborg, 74, prodell, ishizaki));
		elfsborgDjurgarden.addChange(new Change(djurgarden, 65, ulvestad, badji));
		elfsborgDjurgarden.addChange(new Change(djurgarden, 75, kozica, karlstrom));
		elfsborgDjurgarden.addChange(new Change(djurgarden, 85, djurgardenOlsson, kadewere));
	}


	public Game getSingleGame() {
		return sundsvallHammarby;
	}

	public Collection<Game> getAllGames() {
		return Arrays.asList(sundsvallHammarby, elfsborgDjurgarden);
	}
}
