package se.fermitet.jrc.example.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;

@Entity
public class Arena {
	@Attribute
	private String name;
	
	@Attribute
	private String city;
	
	@Attribute
	private int seatMax;

	public Arena() {
		super();
	}
	
	public Arena(String name, String city, int seatMax) {
		super();
		this.name = name;
		this.city = city;
		this.seatMax = seatMax;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getSeatMax() {
		return seatMax;
	}

	public void setSeatMax(int seatMax) {
		this.seatMax = seatMax;
	}
}
