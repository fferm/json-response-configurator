package se.fermitet.jrc.example.webshop.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class Order {
	
	@Attribute
	private Integer orderId;
	
	@Attribute
	private LocalDate date;
	
	@Relation
	private Set<OrderLine> orderLines;
	
	public Order() {
		super();
		orderLines = new HashSet<OrderLine>();
	}
	
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	@Attribute
	public Money getTotalSum() {
		Money res = new Money("0,00");
		for (OrderLine ol : orderLines) {
			res = res.add(ol.getProduct().getUnitPrice().multiply(ol.getQuantity()));
		}
		return res;
	}
	public Set<OrderLine> getOrderLines() {
		return orderLines;
	}
	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
	public void addOrderLine(OrderLine orderLine) {
		this.orderLines.add(orderLine);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("Order ");
		buf.append("{");
		buf.append("orderId: ");
		buf.append(getOrderId());
		buf.append(", date: ");
		buf.append(getDate());
		buf.append(", totalSum: ");
		buf.append(getTotalSum());
		buf.append(", orderLines:");
		buf.append("[");
		for (OrderLine line : getOrderLines()) {
			buf.append(line);
			buf.append(", ");
		}
		buf.append("]");
		buf.append("}");
		
		return buf.toString();
	}

}
