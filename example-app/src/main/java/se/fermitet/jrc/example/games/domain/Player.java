package se.fermitet.jrc.example.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;

@Entity
public class Player {

	@Attribute
	private String name;
	
	@Attribute
	private int number;
	
	public Player() {
		super();
	}
	
	public Player(String name, int number) {
		this();
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	
}
