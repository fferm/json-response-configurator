package se.fermitet.jrc.example.games.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class Game {
	@Attribute
	private Team homeTeam;
	
	@Attribute
	private Team awayTeam;

	@Attribute
	private LocalDateTime timestamp;
	
	@Attribute
	private int numSpectators;

	@Relation
	private Arena arena;

	@Relation
	private Collection<Goal> goals;
	
	@Relation
	private Collection<Card> cards;
	
	@Relation
	private Collection<Change> changes;

	public Game() {
		super();
		this.goals = new ArrayList<Goal>();
		this.cards = new ArrayList<Card>();
		this.changes = new ArrayList<Change>();
	}
	
	public Game(Team homeTeam, Team awayTeam) {
		this();
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
	}


	public Game(Team homeTeam, Team awayTeam, LocalDateTime timestamp, int numSpectators, Arena arena) {
		this(homeTeam, awayTeam);
		this.timestamp = timestamp;
		this.numSpectators = numSpectators;
		this.arena = arena;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public int getNumSpectators() {
		return numSpectators;
	}

	public void setNumSpectators(int numSpectators) {
		this.numSpectators = numSpectators;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Collection<Goal> getGoals() {
		return this.goals;
	}
	
	public void addGoal(Goal g) {
		this.goals.add(g);
	}
	
	public void removeGoal(Goal g) {
		this.goals.remove(g);
	}

	public Collection<Card> getCards() {
		return this.cards;
	}
	
	public void addCard(Card c) {
		this.cards.add(c);
	}
	
	public void removeCard(Card c) {
		this.cards.remove(c);
	}

	public Collection<Change> getChanges() {
		return this.changes;
	}
	
	public void addChange(Change c) {
		this.changes.add(c);
	}
	
	public void removeChange(Change c) {
		this.changes.remove(c);
	}
	

	public Arena getArena() {
		return arena;
	}


	public void setArena(Arena arena) {
		this.arena = arena;
	}


	@Attribute
	public int getHomeGoals() {
		return (int) this.goals.stream()
				.filter(g -> g.getTeam().equals(this.getHomeTeam()))
				.count();
	}
	
	@Attribute
	public int getAwayGoals() {
		return (int) this.goals.stream()
				.filter(g -> g.getTeam().equals(this.getAwayTeam()))
				.count();
	}


	
}
