package se.fermitet.jrc.example.webshop.rest;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.fermitet.jrc.example.rest.DataPackage;
import se.fermitet.jrc.example.webshop.domain.Customer;
import se.fermitet.jrc.example.webshop.data.ExampleData;

@Path("/")
public class WebshopRestClient {
	@Inject
	private ExampleData data;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customers")
	public Response getAllCustomers() {
		Collection<Customer> result = data.getAllCustomers();
		
		return Response.ok().entity(result).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/customers/{customerId}")
	public Response getSingleCustomerById(@PathParam("customerId") Integer customerId) {
		Customer result = data.getCustomerById(customerId);
		
		return Response.ok().entity(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/data")
	public Response getCustomerWithData() {
		DataPackage pck = new DataPackage();
		pck.setPayload(data.getCustomerById(12));
		pck.setStatus(100);
		pck.setMessage("Everything OK");
		
		return Response.ok().entity(pck).build();
	}
	
}
