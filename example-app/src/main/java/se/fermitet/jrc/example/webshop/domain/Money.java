package se.fermitet.jrc.example.webshop.domain;

import java.math.BigDecimal;

public class Money implements Comparable<Money> {

	private BigDecimal bigDecimal;

	public Money(String string) {
		this.bigDecimal = new BigDecimal(string.replace(",", "."));
	}
	
	private Money(BigDecimal bigDecimal) {
		this.bigDecimal = bigDecimal;
	}
	
	@Override
	public String toString() {
		return this.bigDecimal.toString().replace(".",",");
	}

	public Money multiply(int multiplier) {
		return new Money(this.bigDecimal.multiply(new BigDecimal(multiplier)));
	}

	public Money add(Money other) {
		return new Money(this.bigDecimal.add(other.bigDecimal));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bigDecimal == null) ? 0 : bigDecimal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (bigDecimal == null) {
			if (other.bigDecimal != null)
				return false;
		} else if (!bigDecimal.equals(other.bigDecimal))
			return false;
		return true;
	}

	@Override
	public int compareTo(Money other) {
		return this.bigDecimal.compareTo(other.bigDecimal);
	}

	
}
