package se.fermitet.jrc.example.games.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.fermitet.jrc.example.games.data.GamesDataGenerator;
import se.fermitet.jrc.example.games.domain.Game;
import se.fermitet.jrc.example.games.domain.Season;

import java.util.Collection;

@Path("/")
public class GamesRest {
	private GamesDataGenerator gamesDataGenerator;

	public GamesRest() {
		super();
		this.gamesDataGenerator = new GamesDataGenerator();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/game")
	public Response getSingleGame() {
		Game result = gamesDataGenerator.getSingleGame();
		return Response.ok().entity(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/season")
	public Response getSeason() {
		Season season = new Season("Allsvenskan", "2017-2018");
		for (Game game : gamesDataGenerator.getAllGames()) {
			season.addGame(game);
		}

		return Response.ok().entity(season).build();
	}

}
