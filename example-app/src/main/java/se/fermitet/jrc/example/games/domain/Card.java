package se.fermitet.jrc.example.games.domain;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class Card {
	public enum Type {
		RED, YELLOW
	}
	
	@Attribute
	private Type type;
	
	@Attribute
	private int minute;
	
	@Relation 
	private Player player;
	
	@Relation
	private Team team;

	public Card() {
		super();
	}

	public Card(Team team, int minute, Type type, Player player) {
		this();
		this.type = type;
		this.minute = minute;
		this.player = player;
		this.team = team;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
	
	
	
	
}
