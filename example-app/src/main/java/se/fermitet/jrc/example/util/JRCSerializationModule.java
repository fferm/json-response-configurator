package se.fermitet.jrc.example.util;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.module.SimpleModule;

import se.fermitet.jrc.example.webshop.domain.Money;
import se.fermitet.jrc.example.webshop.rest.MoneySerializer;

public class JRCSerializationModule extends SimpleModule {
	private static final long serialVersionUID = -5995638732826871752L;

	public JRCSerializationModule() {
		super("JRCSerializationModule");
		
		addSerializer(Money.class, new MoneySerializer());
		addSerializer(LocalDate.class, new LocalDateSerializer());
		addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
	}

}
