package se.fermitet.jrc.example.webshop.domain;

import java.util.HashSet;
import java.util.Set;

import se.fermitet.jrc.annotation.Attribute;
import se.fermitet.jrc.annotation.Entity;
import se.fermitet.jrc.annotation.Relation;

@Entity
public class Customer {
	@Attribute
	private Integer customerId;
	
	@Attribute
	private String name;
	
	@Attribute
	private String address;
	
	@Relation
	private Set<Order> orders;
	
	public Customer() {
		super();
		this.orders = new HashSet<Order>();
	}
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	public void addOrder(Order order) {
		this.orders.add(order);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();

		buf.append("Customer ");
		buf.append("{");
		buf.append("customerId: ");
		buf.append(getCustomerId());
		buf.append(", name: ");
		buf.append(getName());
		buf.append(", address: ");
		buf.append(getAddress());
		buf.append(", orders:");
		buf.append("[");
		for (Order order : getOrders()) {
			buf.append(order);
			buf.append(", ");
		}
		buf.append("]");
		buf.append("}");
		
		return buf.toString();
	}

	
}
